package concepts;

import java.util.Map;

/**
 * Calculate the odds of winning.
 */
public class OddsCalculator
{
    /**
     * General method for calculating odds. If it only receives one
     * argument, it will simply load the pre-flop odds and invoke
     * the preFlop method.
     * @param cardsOfPlayer
     * @return 
     */
    public static Odds calculateOdds(Hand cardsOfPlayer)
    {
        Map<String,Double> preFlopOdds = Game.loadOddsMap();
        return preFlop(cardsOfPlayer, preFlopOdds);
    }
    
    /**
     * General method for calculating odds. If it receives a hand and the
     * preFlopOdds map, it will simply invoke the preFlopOdds method.
     * @param cardsOfPlayer
     * @param preFlopOdds
     * @return 
     */
    public static Odds calculateOdds(
            Hand cardsOfPlayer, Map<String,Double> preFlopOdds)
    {
        return preFlopOdds(cardsOfPlayer, preFlopOdds);
    }
    
    /**
     * General method for calculating odds. Counts the number of
     * cards on table and invokes the corresponding method.
     * @param cardsOfPlayer
     * @param cardsOnTable
     * @return 
     */
    public static Odds calculateOdds(Hand cardsOfPlayer, Hand cardsOnTable)
    {
        switch(cardsOnTable.getCardCount())
        {
            case 3:
                return flop(cardsOfPlayer, cardsOnTable);
            case 4:
                return turn(cardsOfPlayer, cardsOnTable);
            case 5:
                return river(cardsOfPlayer, cardsOnTable);
            default:
                return calculateOdds(cardsOfPlayer);
        }
    }
    
    /**
     * Wrapper method for preFlopOdds.
     * @param cardsOfPlayer
     * @param preFlopOdds
     * @return 
     */
    public static Odds preFlop(
            Hand cardsOfPlayer, Map<String, Double> preFlopOdds)
    {
        return preFlopOdds(cardsOfPlayer, preFlopOdds);
    }
    
    /**
     * Calculate the odds of winning after flop.
     * @param cardsOfPlayer
     * @param cardsOnTable
     * @return 
     */
    public static Odds flop(Hand cardsOfPlayer, Hand cardsOnTable)
    {
        /* After extensive testing, the values of k1, k2 and k3
         * have been chosen to be 0.6, 0.2 and 0.015 respectively,
         * due to providing the best results in reasonable time
         * based on our empirical research.
         */
        return flopMCOdds(
                cardsOfPlayer, cardsOnTable,0.6,0.2,0.015);
    }
    
    /**
     * Calculate the odds of winning after turn.
     * @param cardsOfPlayer
     * @param cardsOnTable
     * @return 
     */
    public static Odds turn(Hand cardsOfPlayer, Hand cardsOnTable)
    {
        /* After extensive testing, the values of k1 and k2 have been 
         * chosen to be 0.75 and 0.08 respectively, due to providing the 
         * best results in reasonable time based on our empirical research.
         */
        return turnMCOdds(cardsOfPlayer, cardsOnTable,0.9,0.1);
    }
    
    /**
     * Calculate the odds of winning after river.
     * @param cardsOfPlayer
     * @param cardsOnTable
     * @return 
     */
    public static Odds river(Hand cardsOfPlayer, Hand cardsOnTable)
    {
        return riverOdds(cardsOfPlayer, cardsOnTable);
    }
    
    /**
     * Calculate the odds of victory given only the two cards of
     * the player. This is done by looking up a pre-loaded map
     * of probabilities.
     * @param preFlopOdds
     * @param cardsOfPlayer
     * @return 
     */
    public static Odds preFlopOdds(
            Hand cardsOfPlayer, Map<String, Double> preFlopOdds)
    {
        return preFlopOdds(cardsOfPlayer, preFlopOdds, false);
    }
    
    /**
     * Calculate the odds of victory given only the two cards of
     * the player. This is done by looking up a pre-loaded map
     * of probabilities.
     * If print is true, the results are printed to console.
     * @param preFlopOdds
     * @param cardsOfPlayer
     * @param print
     * @return 
     */
    public static Odds preFlopOdds(
            Hand cardsOfPlayer, Map<String, Double> preFlopOdds, boolean print)
    {
        cardsOfPlayer.sortByValue();
        Card first = cardsOfPlayer.getCard(1);
        Card second = cardsOfPlayer.getCard(0);
        String key = buildKey(first,second);
        double prob = preFlopOdds.get(key);
        
        if(print)
        {
            System.out.println("### RESULTS ### (PRE-FLOP)");
            System.out.println("cardsOfPlayer: " + cardsOfPlayer);
            System.out.println("Look-up key = " + key);
            System.out.println(String.format("Probability of winning = %4.2f",prob));
            System.out.println("### ### ### ###");
        }
        
        Odds odds = new Odds(prob, 100 - prob, 0, 169);
        
        return odds;   
    }
    
    /**
     * Auxiliary method for preFlopOdds to construct the key
     * used to look up the winning probability of a card combo
     * in the preFlopOdds Map.
     * @param first
     * @param second
     * @return 
     */
    public static String buildKey(Card first, Card second)
    {
        StringBuilder key = new StringBuilder();        
        Integer [] ranks = new Integer[2];
        ranks[0] = first.getRank();
        ranks[1] = second.getRank();
        
        for(int i = 0; i < 2; i++) {
            switch(ranks[i])
            {
                case 14:
                    key.append("A");
                    break;
                case 13:
                    key.append("K");
                    break;
                case 12:
                    key.append("Q");
                    break;
                case 11:
                    key.append("J");
                    break;
                case 10:
                    key.append("T");
                    break;
                default:
                    key.append(ranks[i].toString());
            }
        }
        
        if (first.getSuit() == second.getSuit())
            key.append("s");
        else if (first.getRank() != second.getRank())
            key.append("o");
        return key.toString();
    }
    
    /**
     * Calculate the odds of victory of a determined combo of player cards
     * and cards on table using a complete search.
     * @param cardsOfPlayer
     * @param cardsOnTable
     * @return 
     */
    public static Odds flopOdds(
            Hand cardsOfPlayer, Hand cardsOnTable)
    {
        return flopOdds(cardsOfPlayer, cardsOnTable, false);
    }
    
    /**
     * Calculate the odds of victory of a determined combo of player cards
     * and cards on table using a complete search. If print variable is
     * true, it will print results in console.
     * @param cardsOfPlayer
     * @param cardsOnTable
     * @param print
     * @return 
     */
    public static Odds flopOdds(
            Hand cardsOfPlayer, Hand cardsOnTable, boolean print)
    {        
        long timeStart, timeEnd;
        Card c1, c2, c3, c4;
        Hand h1, h2, h3, h4, table, otherPlayer;
        Hand tr_combos, opp_combos;
        Deck d1, d2, d3, d4;
        int[] results = new int[3];
        int i = 0, j;
        
        timeStart = System.currentTimeMillis();
        
        h1 = new Hand(5);
        h2 = new Hand(6);
        h3 = new Hand(7);
        h4 = new Hand(8);
        table = new Hand(5);
        otherPlayer = new Hand(2);
        
        tr_combos = new Hand(-1);
        opp_combos = new Hand(-1);
        
        h1.clear();
        h1.addCards(cardsOfPlayer);
        h1.addCards(cardsOnTable);
        d1 = new Deck(h1);
        
        tr_combos.addCards(h1);
        
        while(d1.cardsLeft() > 0)
        {
            c1 = d1.dealCard();

            h2.clear();
            h2.addCards(h1);
            h2.addCard(c1);
            tr_combos.addCard(c1);
            d2 = new Deck(tr_combos);
            while(d2.cardsLeft() > 0)
            {             
                c2 = d2.dealCard();
                
                h3.clear();
                h3.addCards(h2);
                h3.addCard(c2);

                opp_combos.clear();
                opp_combos.addCards(h3);
                d3 = new Deck(h3);
                while(d3.cardsLeft() > 0)
                {
                    c3 = d3.dealCard();

                    h4.clear();
                    h4.addCards(h3);
                    h4.addCard(c3);
                    opp_combos.addCard(c3);
                    d4 = new Deck(opp_combos);
                    while(d4.cardsLeft() > 0)
                    {
                        c4 = d4.dealCard();

                        //evaluate case
                        i++;
                        table.clear();
                        table.addCards(cardsOnTable);
                        table.addCard(c1);
                        table.addCard(c2);
                        otherPlayer.clear();
                        otherPlayer.addCard(c3);
                        otherPlayer.addCard(c4);
                        j = HandEvaluator.getWinner(table, cardsOfPlayer, otherPlayer);
                        results[j]++;
                    }
                }
            }
        }
        
        double won = (double)results[1]*100/i;
        double lost = (double)results[2]*100/i;
        double tied = (double)results[0]*100/i;
        
        if(print)
        {
            timeEnd = System.currentTimeMillis();
            System.out.println(String.format("### RESULTS ### (FLOP = %,d ms)",
                timeEnd - timeStart));
            System.out.println("cardsOfPlayer: " + cardsOfPlayer);
            System.out.println("cardsOnTable:  " + cardsOnTable);
            System.out.println(String.format("%-5s: %,9d", "cases", i));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                    "tied", results[0], tied));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                    "won", results[1], won));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                    "lost", results[2], lost));
            System.out.println("### ### ### ###");
        }
        
        Odds odds = new Odds(won,lost,tied,i);
        
        return odds;
    }
    
    /**
     * Calculate the odds of victory of a determined combo o player cards
     * and cards on table using a randomly trimmed search tree, similar
     * to a Monte Carlo search tree, hence the name. Parameters k1, k2, and k3
     * are the probabilities that the search will go into a branch on levels
     * 1, 2 and 3 respectively.
     * @param cardsOfPlayer
     * @param cardsOnTable
     * @param k1
     * @param k2
     * @param k3
     * @return 
     */
    public static Odds flopMCOdds(
            Hand cardsOfPlayer, Hand cardsOnTable,
            double k1, double k2, double k3)
    {
        return flopMCOdds(cardsOfPlayer, cardsOnTable, k1, k2, k3, false);
    }
    
    /**
     * Calculate the odds of victory of a determined combo o player cards
     * and cards on table using a randomly trimmed search tree, similar
     * to a Monte Carlo search tree, hence the name. Parameters k1, k2, and k3
     * are the probabilities that the search will go into a branch on levels
     * 1, 2 and 3 respectively.
     * If print is true, results will be printed on console.
     * @param cardsOfPlayer
     * @param cardsOnTable
     * @param k1
     * @param k2
     * @param k3
     * @param print
     * @return 
     */
    public static Odds flopMCOdds(
            Hand cardsOfPlayer, Hand cardsOnTable,
            double k1, double k2, double k3, boolean print)
    {        
        long timeStart, timeEnd;
        Card c1, c2, c3, c4;
        Hand h1, h2, h3, h4, table, otherPlayer;
        Hand tr_combos, opp_combos;
        Deck d1, d2, d3, d4;
        int[] results = new int[3];
        int i = 0, j;
        
        double k;
        
        timeStart = System.currentTimeMillis();
        
        h1 = new Hand(5);
        h2 = new Hand(6);
        h3 = new Hand(7);
        h4 = new Hand(8);
        table = new Hand(5);
        otherPlayer = new Hand(2);
        
        tr_combos = new Hand(-1);
        opp_combos = new Hand(-1);
        
        h1.clear();
        h1.addCards(cardsOfPlayer);
        h1.addCards(cardsOnTable);
        d1 = new Deck(h1);
        
        tr_combos.addCards(h1);
        
        while(d1.cardsLeft() > 0)
        {
            c1 = d1.dealCard();

            h2.clear();
            h2.addCards(h1);
            h2.addCard(c1);
            tr_combos.addCard(c1);
            d2 = new Deck(tr_combos);
            while(d2.cardsLeft() > 0)
            {             
                c2 = d2.dealCard();
                
                k = Math.random();
                
                if(k < k1) {
                
                    h3.clear();
                    h3.addCards(h2);
                    h3.addCard(c2);

                    opp_combos.clear();
                    opp_combos.addCards(h3);
                    d3 = new Deck(h3);
                    while(d3.cardsLeft() > 0)
                    {
                        c3 = d3.dealCard();
                        
                        k = Math.random();

                        if(k < k2) {
                            
                            h4.clear();
                            h4.addCards(h3);
                            h4.addCard(c3);
                            opp_combos.addCard(c3);
                            d4 = new Deck(opp_combos);
                            while(d4.cardsLeft() > 0)
                            {
                                c4 = d4.dealCard();

                                k = Math.random();
                                
                                if(k < k3) {
                                    //evaluate case
                                    i++;
                                    table.clear();
                                    table.addCards(cardsOnTable);
                                    table.addCard(c1);
                                    table.addCard(c2);
                                    otherPlayer.clear();
                                    otherPlayer.addCard(c3);
                                    otherPlayer.addCard(c4);
                                    j = HandEvaluator.getWinner(table, cardsOfPlayer, otherPlayer);
                                    results[j]++;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        double won = (double)results[1]*100/i;
        double lost = (double)results[2]*100/i;
        double tied = (double)results[0]*100/i;
        
        if(print) {
            timeEnd = System.currentTimeMillis();
            System.out.println(String.format("### RESULTS ### (FLOP = %,d ms)",
                timeEnd - timeStart));
            System.out.println("cardsOfPlayer: " + cardsOfPlayer);
            System.out.println("cardsOnTable:  " + cardsOnTable);
            System.out.println(String.format("%-5s: %,9d", "cases", i));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                    "tied", results[0], tied));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                "won", results[1], won));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                    "lost", results[2], lost));
            System.out.println("### ### ### ###");
        }
        
        if(i == 0)
        {
            won = 50.0;
            lost = 50.0;
            tied = 0.0;
        }
            
        Odds odds = new Odds(won,lost,tied,i);
        
        return odds;
    }
    
    /**
     * Calculate the odds of victory given the player's cards and the
     * cards on the table after the turn using a complete search.
     * @param cardsOfPlayer
     * @param cardsOnTable
     * @return 
     */
    public static Odds turnOdds(Hand cardsOfPlayer, Hand cardsOnTable)
    {
        return turnOdds(cardsOfPlayer, cardsOnTable, false);
    }
    
    /**
     * Calculate the odds of victory given the player's cards and the
     * cards on the table after the turn using a complete search.
     * If print is true, the results will be printed to console.
     * @param cardsOfPlayer
     * @param cardsOnTable
     * @param print
     * @return 
     */
    public static Odds turnOdds(
            Hand cardsOfPlayer, Hand cardsOnTable, boolean print)
    {
        long timeStart, timeEnd;
        Card c1, c2, c3;
        Hand h1, h2, h3, table, otherPlayer, opp_combos;
        Deck d1, d2, d3;
        int[] results = new int[3];
        int i = 0, j;
        
        timeStart = System.currentTimeMillis();
        
        h1 = new Hand(6);
        h2 = new Hand(7);
        h3 = new Hand(8);
        table = new Hand(5);
        otherPlayer = new Hand(2);
        
        opp_combos = new Hand(-1);

        h1.clear();
        h1.addCards(cardsOfPlayer);
        h1.addCards(cardsOnTable);
        d1 = new Deck(h1);
        while(d1.cardsLeft() > 0)
        {
            c1 = d1.dealCard();
            
            h2.clear();
            h2.addCards(h1);
            h2.addCard(c1);
            opp_combos.clear();
            opp_combos.addCards(h2);
            
            d2 = new Deck(h2);
            while(d2.cardsLeft() > 0)
            {
                c2 = d2.dealCard();
                
                h3.clear();
                h3.addCards(h2);
                h3.addCard(c2);
                opp_combos.addCard(c2);
                d3 = new Deck(opp_combos);
                while(d3.cardsLeft() > 0)
                {
                    c3 = d3.dealCard();
                    
                    //evaluate case
                    i++;
                    table.clear();
                    table.addCards(cardsOnTable);
                    table.addCard(c1);
                    otherPlayer.clear();
                    otherPlayer.addCard(c2);
                    otherPlayer.addCard(c3);
                    j = HandEvaluator.getWinner(table, cardsOfPlayer, otherPlayer);
                    results[j]++;
                }
            }
        }
        
        double won = (double)results[1]*100/i;
        double lost = (double)results[2]*100/i;
        double tied = (double)results[0]*100/i;
        
        if(print) {
            timeEnd = System.currentTimeMillis();
            System.out.println(String.format("### RESULTS ### (TURN = %,d ms)",
                    timeEnd - timeStart));
            System.out.println("cardsOfPlayer: " + cardsOfPlayer);
            System.out.println("cardsOnTable:  " + cardsOnTable);
            System.out.println(String.format("%-5s: %,9d", "cases", i));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                    "tied", results[0], tied));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                    "won", results[1], won));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                    "lost", results[2], lost));
            System.out.println("### ### ### ###");
        }
        
        Odds odds = new Odds(won,lost,tied,i);
        
        return odds;
    }
    
    /**
     * Calculate the odds of victory given the player's cards and the
     * cards on the table after the turn using a pseudo Monte Carlo search.
     * @param cardsOfPlayer
     * @param cardsOnTable
     * @param k1
     * @param k2
     * @return 
     */
    public static Odds turnMCOdds(
            Hand cardsOfPlayer, Hand cardsOnTable, 
            double k1, double k2)
    {
        return turnMCOdds(cardsOfPlayer, cardsOnTable, k1, k2, false);
    }
    
    /**
     * Calculate the odds of victory given the player's cards and the
     * cards on the table after the turn using a pseudo Monte Carlo search.
     * If print is true, the results will be printed to console.
     * @param cardsOfPlayer
     * @param cardsOnTable
     * @param k1
     * @param k2
     * @param print
     * @return 
     */
    public static Odds turnMCOdds(
            Hand cardsOfPlayer, Hand cardsOnTable, 
            double k1, double k2, boolean print)
    {
        long timeStart, timeEnd;
        Card c1, c2, c3;
        Hand h1, h2, h3, table, otherPlayer, opp_combos;
        Deck d1, d2, d3;
        int[] results = new int[3];
        int i = 0, j;
        
        double k;
        
        timeStart = System.currentTimeMillis();
        
        h1 = new Hand(6);
        h2 = new Hand(7);
        h3 = new Hand(8);
        table = new Hand(5);
        otherPlayer = new Hand(2);
        
        opp_combos = new Hand(-1);

        h1.clear();
        h1.addCards(cardsOfPlayer);
        h1.addCards(cardsOnTable);
        d1 = new Deck(h1);
        while(d1.cardsLeft() > 0)
        {
            c1 = d1.dealCard();
            
            h2.clear();
            h2.addCards(h1);
            h2.addCard(c1);
            opp_combos.clear();
            opp_combos.addCards(h2);
            
            d2 = new Deck(h2);
            while(d2.cardsLeft() > 0)
            {
                c2 = d2.dealCard();
                
                k = Math.random();
                
                if(k < k1)
                {
                    h3.clear();
                    h3.addCards(h2);
                    h3.addCard(c2);
                    opp_combos.addCard(c2);
                    d3 = new Deck(opp_combos);
                    while(d3.cardsLeft() > 0)
                    {
                        c3 = d3.dealCard();
                        
                        k = Math.random();
                        
                        if(k < k2)
                        {
                            //evaluate case
                            i++;
                            table.clear();
                            table.addCards(cardsOnTable);
                            table.addCard(c1);
                            otherPlayer.clear();
                            otherPlayer.addCard(c2);
                            otherPlayer.addCard(c3);
                            j = HandEvaluator.getWinner(table, cardsOfPlayer, otherPlayer);
                            results[j]++;
                        }
                    }
                }
            }
        }
        
        double won = (double)results[1]*100/i;
        double lost = (double)results[2]*100/i;
        double tied = (double)results[0]*100/i;
        
        if(print) {
            timeEnd = System.currentTimeMillis();
            System.out.println(String.format("### RESULTS ### (TURN = %,d ms)",
                    timeEnd - timeStart));
            System.out.println("cardsOfPlayer: " + cardsOfPlayer);
            System.out.println("cardsOnTable:  " + cardsOnTable);
            System.out.println(String.format("%-5s: %,9d", "cases", i));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                    "tied", results[0], tied));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                    "won", results[1], won));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                    "lost", results[2], lost));
            System.out.println("### ### ### ###");
        }
        
        Odds odds = new Odds(won,lost,tied,i);
        
        return odds;
    }
    
    /**
     * Calculate the odds of victory given the player's cards and the
     * cards on the table after the river using a complete search.
     * @param cardsOfPlayer
     * @param cardsOnTable
     * @return 
     */
    public static Odds riverOdds(Hand cardsOfPlayer, Hand cardsOnTable)
    {
        return riverOdds(cardsOfPlayer, cardsOnTable, false);
    }
    
    /**
     * Calculate the odds of victory given the player's cards and the
     * cards on the table after the river using a complete search.
     * If print is true, the results will be printed to console.
     * @param cardsOfPlayer
     * @param cardsOnTable
     * @param print
     * @return 
     */
    public static Odds riverOdds(
            Hand cardsOfPlayer, Hand cardsOnTable, boolean print)
    {
        long timeStart, timeEnd;
        Card c1, c2;
        Hand h1, h2, otherPlayer, opp_combos;
        Deck d1, d2;
        int[] results = new int[3];
        int i = 0, j;
        
        timeStart = System.currentTimeMillis();
        
        h1 = new Hand(7);
        h2 = new Hand(8);
        otherPlayer = new Hand(2);
        
        opp_combos = new Hand(-1);
        
        h1.clear();
        h1.addCards(cardsOfPlayer);
        h1.addCards(cardsOnTable);
        opp_combos.addCards(h1);
        d1 = new Deck(h1);
        while(d1.cardsLeft() > 0)
        {
            c1 = d1.dealCard();
            
            h2.clear();
            h2.addCards(h1);
            h2.addCard(c1);
            opp_combos.addCard(c1);
            d2 = new Deck(opp_combos);
            while(d2.cardsLeft() > 0)
            {
                c2 = d2.dealCard();
                
                //evaluate case
                i++;
                otherPlayer.clear();
                otherPlayer.addCard(c1);
                otherPlayer.addCard(c2);
                j = HandEvaluator.getWinner(cardsOnTable, cardsOfPlayer, otherPlayer);
                results[j]++;
            }
        }
        
        double won = (double)results[1]*100/i;
        double lost = (double)results[2]*100/i;
        double tied = (double)results[0]*100/i;
        
        if(print) {
            timeEnd = System.currentTimeMillis();
            System.out.println(String.format("### RESULTS ### (RIVER = %,d ms)",
                    timeEnd - timeStart));
            System.out.println("cardsOfPlayer: " + cardsOfPlayer);
            System.out.println("cardsOnTable:  " + cardsOnTable);
            System.out.println(String.format("%-5s: %,9d", "cases", i));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                    "tied", results[0], tied));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                    "won", results[1], won));
            System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                    "lost", results[2], lost));
            System.out.println("### ### ### ###");
        }
 
        Odds odds = new Odds(won,lost,tied,i);
        
        return odds;
    }
    
    /**
     * Deprecated method. Kept for historical reasons.
     * @param cardsOfPlayer
     * @param cardsOnTable 
     */
    public static void b(Hand cardsOfPlayer, Hand cardsOnTable)
    {
        long timeStart, timeEnd;
        Card c1, c2, c3, c4;
        Hand h1, h2, h3, h4, table, otherPlayer;
        Deck d1, d2, d3, d4;
        int[] results = new int[3];
        int i = 0, j;
        
        timeStart = System.currentTimeMillis();
        
        h1 = new Hand(5);
        h2 = new Hand(6);
        h3 = new Hand(7);
        h4 = new Hand(8);
        table = new Hand(5);
        otherPlayer = new Hand(2);
        
        
        h1.clear();
        h1.addCards(cardsOfPlayer);
        h1.addCards(cardsOnTable);
        d1 = new Deck(h1);
        while(d1.cardsLeft() > 0)
        {
            c1 = d1.dealCard();
            
            h2.clear();
            h2.addCards(h1);
            h2.addCard(c1);
            d2 = new Deck(h2);
            while(d2.cardsLeft() > 0)
            {
                c2 = d2.dealCard();
                
                h3.clear();
                h3.addCards(h2);
                h3.addCard(c2);
                d3 = new Deck(h3);
                while(d3.cardsLeft() > 0)
                {
                    c3 = d3.dealCard();
                    
                    h4.clear();
                    h4.addCards(h3);
                    h4.addCard(c3);
                    d4 = new Deck(h4);
                    while(d4.cardsLeft() > 0)
                    {
                        c4 = d4.dealCard();
                        
                        //evaluate case
                        i++;
                        table.clear();
                        table.addCards(cardsOnTable);
                        table.addCard(c1);
                        table.addCard(c2);
                        otherPlayer.clear();
                        otherPlayer.addCard(c3);
                        otherPlayer.addCard(c4);
                        j = HandEvaluator.getWinner(table, cardsOfPlayer, otherPlayer);
                        results[j]++;
                    }
                }
            }
        }
        timeEnd = System.currentTimeMillis();
        System.out.println(String.format("### RESULTS ### (FLOP = %,d ms)",
                timeEnd - timeStart));
        System.out.println("cardsOfPlayer: " + cardsOfPlayer);
        System.out.println("cardsOnTable:  " + cardsOnTable);
        System.out.println(String.format("%-5s: %,9d", "cases", i));
        System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                "tied", results[0], (double)results[0]*100/i));
        System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                "won", results[1], (double)results[1]*100/i));
        System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                "lost", results[2], (double)results[2]*100/i));
        System.out.println("### ### ### ###");
    }
    
    /**
     * Deprecated. Kept for historical reasons.
     * @param cardsOfPlayer
     * @param cardsOnTable 
     */
    public static void c(Hand cardsOfPlayer, Hand cardsOnTable)
    {
        long timeStart, timeEnd;
        Card c1, c2, c3;
        Hand h1, h2, h3, table, otherPlayer;
        Deck d1, d2, d3;
        int[] results = new int[3];
        int i = 0, j;
        
        timeStart = System.currentTimeMillis();
        
        h1 = new Hand(6);
        h2 = new Hand(7);
        h3 = new Hand(8);
        table = new Hand(5);
        otherPlayer = new Hand(2);
        
        
        h1.clear();
        h1.addCards(cardsOfPlayer);
        h1.addCards(cardsOnTable);
        d1 = new Deck(h1);
        while(d1.cardsLeft() > 0)
        {
            c1 = d1.dealCard();
            
            h2.clear();
            h2.addCards(h1);
            h2.addCard(c1);
            d2 = new Deck(h2);
            while(d2.cardsLeft() > 0)
            {
                c2 = d2.dealCard();
                
                h3.clear();
                h3.addCards(h2);
                h3.addCard(c2);
                d3 = new Deck(h3);
                while(d3.cardsLeft() > 0)
                {
                    c3 = d3.dealCard();
                    
                    //evaluate case
                    i++;
                    table.clear();
                    table.addCards(cardsOnTable);
                    table.addCard(c1);
                    otherPlayer.clear();
                    otherPlayer.addCard(c2);
                    otherPlayer.addCard(c3);
                    j = HandEvaluator.getWinner(table, cardsOfPlayer, otherPlayer);
                    results[j]++;
                }
            }
        }
        timeEnd = System.currentTimeMillis();
        System.out.println(String.format("### RESULTS ### (TURN = %,d ms)",
                timeEnd - timeStart));
        System.out.println("cardsOfPlayer: " + cardsOfPlayer);
        System.out.println("cardsOnTable:  " + cardsOnTable);
        System.out.println(String.format("%-5s: %,9d", "cases", i));
        System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                "tied", results[0], (double)results[0]*100/i));
        System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                "won", results[1], (double)results[1]*100/i));
        System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                "lost", results[2], (double)results[2]*100/i));
        System.out.println("### ### ### ###");
    }
    
    /**
     * Deprecated. Kept for historical reasons.
     * @param cardsOfPlayer
     * @param cardsOnTable 
     */
    public static void d(Hand cardsOfPlayer, Hand cardsOnTable)
    {
        long timeStart, timeEnd;
        Card c1, c2;
        Hand h1, h2, otherPlayer;
        Deck d1, d2;
        int[] results = new int[3];
        int i = 0, j;
        
        timeStart = System.currentTimeMillis();
        
        h1 = new Hand(7);
        h2 = new Hand(8);
        otherPlayer = new Hand(2);
        
        
        h1.clear();
        h1.addCards(cardsOfPlayer);
        h1.addCards(cardsOnTable);
        d1 = new Deck(h1);
        while(d1.cardsLeft() > 0)
        {
            c1 = d1.dealCard();
            
            h2.clear();
            h2.addCards(h1);
            h2.addCard(c1);
            d2 = new Deck(h2);
            while(d2.cardsLeft() > 0)
            {
                c2 = d2.dealCard();
                
                //evaluate case
                i++;
                otherPlayer.clear();
                otherPlayer.addCard(c1);
                otherPlayer.addCard(c2);
                j = HandEvaluator.getWinner(cardsOnTable, cardsOfPlayer, otherPlayer);
                results[j]++;
            }
        }
        timeEnd = System.currentTimeMillis();
        System.out.println(String.format("### RESULTS ### (RIVER = %,d ms)",
                timeEnd - timeStart));
        System.out.println("cardsOfPlayer: " + cardsOfPlayer);
        System.out.println("cardsOnTable:  " + cardsOnTable);
        System.out.println(String.format("%-5s: %,9d", "cases", i));
        System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                "tied", results[0], (double)results[0]*100/i));
        System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                "won", results[1], (double)results[1]*100/i));
        System.out.println(String.format("%-4s = %,9d (%4.1f %%)",
                "lost", results[2], (double)results[2]*100/i));
        System.out.println("### ### ### ###");
    }
}
