# Poker

---

**Autores:** Haiko Eitzen, Nico Epp, Ralf Funk y Katherine Vera

**Fecha de inicio:** agosto 2015

---

Realizado en el marco de la materia **Inteligencia Artificial**

de la carrera **Ingeniería en Informática**

de la **Facultad Politécnica**

de la **Universidad Nacional de Asunción**

---

**Requerimientos:** Java SE 7

---

**Modo de uso**

**Paso 1: obtener la aplicación**

1. Descargar los archivos fuente el proyecto hecho en Netbeans 8 (utiliza gerenciador de proyectos Ant) y construir la aplicación (clean and build que genera el .jar).

2. Alternativamente se puede descargar un zip en la sección de Downloads que ya contiene el .jar construido.

**Paso 2: correr la aplicación**

1. Para iniciar el juego de Poker con la GUI (persona vs PC), correr el script "runPoker.bat" (básicamente solo ejecuta el main del .jar).

2. Para simular un juego entre dos jugadores operados por la PC, correr el script "runPoker.bat" pasandole un "1" como argumento en la linea de comando (para argumentos adicionales consulte la documentación dentro del archivo "concepts/GameRunner.java").