package concepts;

import enums.ActionType;
import enums.BettingRoundType;

/**
 * The AI strategy for a poker player.
 */
public class Strategy
{
    /**
     * AI.
     * @param odds
     * @param type
     * @param cardsOnTable
     * @param cardsOfPlayer
     * @param strategy
     * @param thisCase
     * @param qtyRaises
     * @return 
     */
    public static ActionType getRecomendedAction(Odds odds, BettingRoundType type,
            Hand cardsOnTable, Hand cardsOfPlayer, int strategy, int thisCase,
            int qtyRaises)
    {
        ActionType at;
        
        if(strategy == Player.STRATEGY_1)
        {
            at = StrategyOne.getRecomendedAction(odds, type, cardsOnTable,
                    cardsOfPlayer, thisCase, qtyRaises);
        }
        else if(strategy == Player.STRATEGY_2)
        {
            at = StrategyTwo.getRecomendedAction(type, cardsOnTable, cardsOfPlayer,
                    thisCase);
        }
        else
        {
            at = StrategyThree.getRecomendedAction(type, cardsOnTable, cardsOfPlayer,
                    thisCase);
        }
        
        return at;
    }
}
