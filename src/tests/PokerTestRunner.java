package tests;

import concepts.Deck;
import concepts.Game;
import concepts.Hand;
import concepts.Odds;
import concepts.OddsCalculator;
import concepts.Player;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import utils.Logger;

/**
 * Runs the tests.
 * @author Ralf
 */
public class PokerTestRunner
{
    public static void main(String[] args)
    {
        int a = 6;
        switch(a)
        {
            case 1:
                testMethod1();
                break;
            case 2:
                testMethod2();
                break;
            case 3:
                testMethod3();
                break;
            case 4:
                testMethod4();
                break;
            case 5:
                testMethod5();
                break;
            case 6:
                testMethod6();
                break;
            case 7:
                testMethod7();
                break;
            case 8:
                testMethod8();
                break;
            case 9:
                testMethod5();
                testMethod8();
                break;
            default:
        }
    }
    
    public static void testMethod1()
    {
        Class[] testClasses = {HandEvaluatorTest.class};

        Result result = JUnitCore.runClasses(testClasses);
        if (result.getFailures().isEmpty())
        {
            System.out.println("Success: no failures were thrown.");
        }
        else
        {
            for (Failure failure: result.getFailures())
            {
                System.out.println(failure.toString());
            }
        }
    }
    
    public static void testMethod2()
    {
        //the first option is the correct way to test (apparently)
        //but I prefer this because the other doesnt give much info
        //when there is a failure
        HandEvaluatorTest x = new HandEvaluatorTest();
        x.testEvaluation();
        x.testDeterminationOfWinners();
        System.out.println("Success: no failures were thrown.");
    }
    
    public static void testMethod3()
    {
        List<Player> list = new ArrayList<>();
        list.add( new Player("p1", Player.STRATEGY_1) );
        list.add( new Player("p2", Player.STRATEGY_1) );

        Game g = new Game(new Logger(""), list);
        g.play(200);
    }
    
    public static void testMethod4()
    {
        long timeStart1,timeEnd1,timeStart2,timeEnd2,timeStart3,timeEnd3;
        Double won1,won2,won3,varianza1 = 0.0, varianza2 = 0.0;
        Odds odds;

        for(int j = 0; j < 10; j++)
        {
            System.out.println("--- Test # " + j + " ---");
            Hand playerCards = new Hand(2);
            Hand table = new Hand(3);
            Deck deck = new Deck();
            for(int i = 0; i < 2; i++)
                playerCards.addCard(deck.dealCard());
            for(int i = 0; i < 3; i++)
                table.addCard(deck.dealCard());

            System.out.println("Player " + playerCards);
            System.out.println("Table " + table);

            timeStart1 = System.currentTimeMillis();
            odds = OddsCalculator.flopOdds(playerCards,table);
            won1 = odds.getWins();
            timeEnd1 = System.currentTimeMillis();

            System.out.println(String.format("Complete Search won = %4.2f",won1));
            System.out.println(String.format("Time %,d ms",timeEnd1 - timeStart1));

            timeStart2 = System.currentTimeMillis();
            won2 = 0.0;
            for(int i = 0; i < 100; i++)
            {
                odds = OddsCalculator.flopMCOdds(
                        playerCards, table, 0.3, 0.02, 0.001);
                won2 += odds.getWins();
            }
            won2 /= 100;
            timeEnd2 = System.currentTimeMillis();
            varianza1 += Math.pow((won1 - won2),2);

            System.out.println(String.format("Several iterations of MC won = %4.2f",won2));
            System.out.println(String.format("Time %,d ms",timeEnd2 - timeStart2));

            timeStart3 = System.currentTimeMillis();
            odds = OddsCalculator.flopMCOdds(playerCards, table, 0.6, 0.2, 0.015);
            won3 = odds.getWins();
            timeEnd3 = System.currentTimeMillis();
            varianza2 += Math.pow((won1 - won3),2);

            System.out.println(String.format("One (larger) iteration of MC won = %4.2f",won3));
            System.out.println(String.format("%,d evals in %,d ms",
                    odds.getNodes(),timeEnd3 - timeStart3));
            System.out.println("----------------");             
        }
        varianza1 /= 10;
        varianza2 /= 10;

        System.out.println(String.format("Varianza SI %4.2f",varianza1));
        System.out.println(String.format("Varianza 1I %4.2f",varianza2));
    }
    
    public static void testMethod5()
    {
        long[][][] timePerformance;
        timePerformance = new long[5][5][5];

        long timeStart,timeEnd;
        Hand playerCards = new Hand(2);
        Hand table = new Hand(3);
        Deck deck;
        double exactWon,pmcWon;
        Odds odds;

        double[] k1,k2,k3;
        k1 = new double[5];
        k2 = new double[5];
        k3 = new double[5];
        for(int i = 0; i < 5; i++)
        {
            k1[i] = 0.3 + i*0.1;
            k2[i] = 0.2 + i*0.05;
            k3[i] = 0.005 + i*0.005;
        }
        
        double[][][] varianza;
        varianza = new double[5][5][5];
        
        try {
            try (BufferedWriter out = 
                    new BufferedWriter(new FileWriter("results_flop.txt")))
            {
                for(int i = 1; i <= 100; i++)
                {
                    playerCards.clear();
                    table.clear();
                    deck = new Deck();
                    for(int j = 0; j < 2; j++)
                        playerCards.addCard(deck.dealCard());
                    for(int j = 0; j < 3; j++)
                        table.addCard(deck.dealCard());

                    System.out.println(String.format("---- Test # %03d ----",i));
                    System.out.println("Player: " + playerCards);
                    System.out.println("Table: " + table);

                    out.write(String.format("---- Test # %03d ----",i));
                    out.newLine();
                    out.write("Player: " + playerCards);
                    out.newLine();
                    out.write("Table: " + table);
                    out.newLine();

                    odds = OddsCalculator.flopOdds(playerCards,table);
                    exactWon = odds.getWins();

                    System.out.println(String.format("Complete Search -> %4.2f win",exactWon));

                    out.write(String.format("Complete Search -> %4.2f win",exactWon));
                    out.newLine();
                    out.write("Pseudo Monte Carlo searches");
                    out.newLine();

                    for(int j = 0; j < 5; j++)
                    {
                        for(int k = 0; k < 5; k++)
                        {
                            for(int l = 0; l < 5; l++)
                            {
                                timeStart = System.currentTimeMillis();
                                odds = OddsCalculator.flopMCOdds(
                                        playerCards, table, k1[j], k2[k], k3[l]);
                                pmcWon = odds.getWins();
                                timeEnd = System.currentTimeMillis();

                                out.write(String.format(
                                        "k1 = %4.2f\tk2 = %4.2f\tk3 = %5.3f",
                                        k1[j],k2[k],k3[l]));
                                out.write(String.format(
                                        "\t-> %4.2f win, ",pmcWon));
                                out.write(String.format(
                                        "%4.2f off",
                                        Math.abs(exactWon - pmcWon)));
                                out.write(String.format(
                                        "\tprocessed in %,d ms",
                                        timeEnd - timeStart));
                                out.newLine();

                                if(i == 1)
                                {
                                    varianza[j][k][l] = 0.0;
                                    timePerformance[j][k][l] = 0;
                                }
                                timePerformance[j][k][l] += (timeEnd - timeStart);
                                varianza[j][k][l] += (Math.pow((pmcWon - exactWon),2));
                            }
                        }
                    }
                    out.write("---- ---- ---- ---- ----");
                    out.newLine();
                }

                double minVar = 100.0;
                double[] bestK = new double[3];
                long bestTime = 0;
                for(int j = 0; j < 5; j++)
                {
                    for(int k = 0; k < 5; k++)
                    {
                        for(int l = 0; l < 5; l++)
                        {
                            timePerformance[j][k][l] /= 100;
                            varianza[j][k][l] /= 100;
                            if(varianza[j][k][l] < minVar &&
                                    timePerformance[j][k][l] <= 100)
                            {
                                minVar = varianza[j][k][l];
                                bestK[0] = k1[j];
                                bestK[1] = k2[k];
                                bestK[2] = k3[l];
                                bestTime = timePerformance[j][k][l];
                            }
                        }
                    }
                }
                out.newLine();

                out.write("Best K combo found with avg time performance up to 100 ms:");
                out.newLine();
                out.write(String.format(
                        "k1 = %4.2f\tk2 = %4.2f\tk3 = %5.3f",
                        bestK[0],bestK[1],bestK[2]));
                out.newLine();
                out.write(String.format(
                        "processed in avg %,d ms with avg %4.2f var",
                        bestTime,minVar));

                out.newLine();
                out.newLine();

                double [] avgVarJ = new double[5];
                double [] avgVarK = new double[5];
                double [] avgVarL = new double[5];

                for(int j = 0; j < 5; j++)
                {
                    avgVarJ[j] = 0.0;
                    avgVarK[j] = 0.0;
                    avgVarL[j] = 0.0;
                    for(int k = 0; k < 5; k++)
                    {
                        for(int l = 0; l < 5; l++)
                        {
                            avgVarJ[j] += varianza[j][k][l];
                            avgVarK[j] += varianza[k][j][l];
                            avgVarL[j] += varianza[l][k][j];
                        }
                    }
                    avgVarJ[j] /= 25;
                    avgVarK[j] /= 25;
                    avgVarL[j] /= 25;
                }

                out.write("Average var for k1\t");
                for(int i = 0; i < 5; i++)
                {
                    out.write(String.format(
                            "-- %4.2f var %4.2f\t",
                            k1[i],avgVarJ[i]));
                }
                out.newLine();

                out.write("Average var for k2\t");
                for(int i = 0; i < 5; i++)
                {
                    out.write(String.format(
                            "-- %4.2f var %4.2f\t",
                            k2[i],avgVarK[i]));
                }
                out.newLine();

                out.write("Average var for k3\t");
                for(int i = 0; i < 5; i++)
                {
                    out.write(String.format(
                            "-- %5.3f var %4.2f\t",
                            k3[i],avgVarL[i]));
                }
                out.newLine();
                out.close();
            }
            System.out.println("DONE");
        } catch(Exception e) {
            System.out.println(e);
        }
    }
    
    public static void testMethod6()
    {
        Hand playerCards = new Hand(2);
        Hand table = new Hand(5);
        Deck deck;
        Odds odds;
        
        double preflopOdds,flopOdds,turnOdds,riverOdds;
        long timeStart,timeEnd;

        for(int i = 0; i < 10; i++)
        {
            System.out.println("--- Case " + i + " ---");
            playerCards.clear();
            table.clear();
            deck = new Deck();

            for(int j = 0; j < 2; j++)
                playerCards.addCard(deck.dealCard());

            System.out.println("Player: " + playerCards);
            timeStart = System.currentTimeMillis();
            odds = OddsCalculator.calculateOdds(playerCards);
            preflopOdds = odds.getWins();
            timeEnd = System.currentTimeMillis();
            System.out.print(String.format("Odds of winning are %4.2f ",preflopOdds));
            System.out.println(String.format("processed in %,d ms",timeEnd - timeStart));

            for(int j = 0; j < 3; j++)
                table.addCard(deck.dealCard());
            System.out.println("Table after flop: " + table);
            timeStart = System.currentTimeMillis();
            odds = OddsCalculator.flop(playerCards, table);
            flopOdds = odds.getWins();
            timeEnd = System.currentTimeMillis();
            System.out.print(String.format("Odds of winning are %4.2f ",flopOdds));
            System.out.println(String.format("%,d evals in %,d ms",
                    odds.getNodes(),timeEnd - timeStart));

            table.addCard(deck.dealCard());
            System.out.println("Table after turn: " + table);
            timeStart = System.currentTimeMillis();
            odds = OddsCalculator.turn(playerCards, table);
            turnOdds = odds.getWins();
            timeEnd = System.currentTimeMillis();
            System.out.print(String.format("Odds of winning are %4.2f ",turnOdds));
            System.out.println(String.format("%,d evals in %,d ms",
                    odds.getNodes(),timeEnd - timeStart));

            table.addCard(deck.dealCard());
            System.out.println("Table after river: " + table);
            timeStart = System.currentTimeMillis();
            odds = OddsCalculator.river(playerCards, table);
            riverOdds = odds.getWins();
            timeEnd = System.currentTimeMillis();
            System.out.print(String.format("Odds of winning are %4.2f ",riverOdds));
            System.out.println(String.format("%,d evals in %,d ms",
                    odds.getNodes(),timeEnd - timeStart));
        }    
    }
    
    public static void testMethod7()
    {
        Hand playerCards = new Hand(2);
        Hand table = new Hand(5);
        Deck deck;
        Odds odds;
        
        long timeStart,timeEnd;
        
        playerCards.clear();
        table.clear();
        deck = new Deck();

        for(int j = 0; j < 2; j++)
            playerCards.addCard(deck.dealCard());
        
        for(int j = 0; j < 4; j++)
            table.addCard(deck.dealCard());
        
        System.out.println("Player: " + playerCards);
        System.out.println("Table: " + table);
        odds = OddsCalculator.turnOdds(playerCards, table, true);
        System.out.println("k1 = 0.8 and k2 = 0.5");
        odds = OddsCalculator.turnMCOdds(playerCards, table, 0.8, 0.05, true);
        System.out.println("k1 = 0.7 and k2 = 0.5");
        odds = OddsCalculator.turnMCOdds(playerCards, table, 0.7, 0.05, true);
        System.out.println("k1 = 0.6 and k2 = 0.5");
        odds = OddsCalculator.turnMCOdds(playerCards, table, 0.6, 0.05, true);
        System.out.println("k1 = 0.8 and k2 = 0.4");
        odds = OddsCalculator.turnMCOdds(playerCards, table, 0.8, 0.04, true);
        System.out.println("k1 = 0.8 and k2 = 0.3");
        odds = OddsCalculator.turnMCOdds(playerCards, table, 0.8, 0.03, true);
    }
    
    public static void testMethod8()
    {
        long[][] timePerformance;
        timePerformance = new long[10][10];

        long timeStart,timeEnd;
        Hand playerCards = new Hand(2);
        Hand table = new Hand(4);
        Deck deck;
        double exactWon,pmcWon;
        Odds odds;

        double[] k1,k2;
        k1 = new double[10];
        k2 = new double[10];
        for(int i = 0; i < 10; i++)
        {
            k1[i] = 0.4 + i*0.05;
            k2[i] = 0.01 + i*0.01;
        }
        
        double[][] varianza;
        varianza = new double[10][10];
        
        try {
            try (BufferedWriter out = 
                    new BufferedWriter(new FileWriter("results_turn.txt")))
            {
                for(int i = 1; i <= 100; i++)
                {
                    playerCards.clear();
                    table.clear();
                    deck = new Deck();
                    for(int j = 0; j < 2; j++)
                        playerCards.addCard(deck.dealCard());
                    for(int j = 0; j < 4; j++)
                        table.addCard(deck.dealCard());

                    System.out.println(String.format("---- Test # %03d ----",i));
                    System.out.println("Player: " + playerCards);
                    System.out.println("Table: " + table);

                    out.write(String.format("---- Test # %03d ----",i));
                    out.newLine();
                    out.write("Player: " + playerCards);
                    out.newLine();
                    out.write("Table: " + table);
                    out.newLine();

                    odds = OddsCalculator.turn(playerCards,table);
                    exactWon = odds.getWins();

                    System.out.println(String.format("Complete Search -> %4.2f win",exactWon));

                    out.write(String.format("Complete Search -> %4.2f win",exactWon));
                    out.newLine();
                    out.write("Pseudo Monte Carlo searches");
                    out.newLine();

                    for(int j = 0; j < 10; j++)
                    {
                        for(int k = 0; k < 10; k++)
                        {
                            timeStart = System.currentTimeMillis();
                            odds = OddsCalculator.turnMCOdds(
                                    playerCards, table, k1[j], k2[k]);
                            pmcWon = odds.getWins();
                            timeEnd = System.currentTimeMillis();

                            out.write(String.format(
                                    "k1 = %4.2f\tk2 = %5.3f",
                                    k1[j],k2[k]));
                            out.write(String.format(
                                    "\t-> %4.2f win, ",pmcWon));
                            out.write(String.format(
                                    "%4.2f off",
                                    Math.abs(exactWon - pmcWon)));
                            out.write(String.format(
                                    "\tprocessed in %,d ms",
                                    timeEnd - timeStart));
                            out.newLine();

                            if(i == 1)
                            {
                                varianza[j][k] = 0.0;
                                timePerformance[j][k] = 0;
                            }
                            timePerformance[j][k] += (timeEnd - timeStart);
                            varianza[j][k] += (Math.pow((pmcWon - exactWon),2));
                        }
                    }
                    out.write("---- ---- ---- ---- ----");
                    out.newLine();
                }

                double minVar = 100.0;
                double[] bestK = new double[2];
                long bestTime = 0;
                for(int j = 0; j < 10; j++)
                {
                    for(int k = 0; k < 10; k++)
                    {
                        timePerformance[j][k] /= 100;
                        varianza[j][k] /= 100;
                        if(varianza[j][k] < minVar &&
                                timePerformance[j][k] <= 100)
                        {
                            minVar = varianza[j][k];
                            bestK[0] = k1[j];
                            bestK[1] = k2[k];
                            bestTime = timePerformance[j][k];
                        }
                    }
                }
                out.newLine();

                out.write("Best K combo found with time performance up to 100 ms:");
                out.newLine();
                out.write(String.format("k1 = %4.2f\tk2 = %5.3f",bestK[0],bestK[1]));
                out.newLine();
                out.write(String.format(
                        "processed in avg %,d ms with avg %4.2f var",
                        bestTime,minVar));

                out.newLine();
                out.newLine();

                double [] avgVarJ = new double[10];
                double [] avgVarK = new double[10];

                for(int j = 0; j < 10; j++)
                {
                    avgVarJ[j] = 0.0;
                    avgVarK[j] = 0.0;
                    for(int k = 0; k < 10; k++)
                    {
                        avgVarJ[j] += varianza[j][k];
                        avgVarK[j] += varianza[k][j];
                    }
                    avgVarJ[j] /= 10;
                    avgVarK[j] /= 10;
                }

                out.write("Average var for k1\t");
                for(int i = 0; i < 10; i++)
                {
                    out.write(String.format(
                            "-- %4.2f var %4.2f\t",
                            k1[i],avgVarJ[i]));
                }
                out.newLine();

                out.write("Average var for k2\t");
                for(int i = 0; i < 10; i++)
                {
                    out.write(String.format(
                            "-- %5.3f var %5.3f\t",
                            k2[i],avgVarK[i]));
                }
                out.newLine();
                out.close();
            }
            System.out.println("DONE");
        } catch(Exception e) {
            System.out.println(e);
        }
    }
}
 