package concepts;

import enums.BettingRoundType;
import enums.Combination;
import gui.PokerGUI;
import java.util.Arrays;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import utils.Logger;

/**
 * A round.
 * @author Ralf
 */
public class Round
{
    /**
     * Attributes.
     */
    private final Logger logger;
    private final List<Player> players;
    private final Map<String, Double> preFlopOdds;
    private final int roundNumber;
    private final Deck deck;
    private final Hand cardsOnTable;
    private int pot;
    private final boolean withGUI;
    private final JLabel labelRound,
            labelPot,
            labelAmountToCall;
    private final List<JLabel> cardLabels;
    
    /**
     * Constructor.
     * @param logger
     * @param players
     * @param preFlopOdds
     * @param roundNumber
     * @param labelRound
     * @param cardLabels
     * @param labelPot
     * @param labelAmountToCall
     */
    public Round(Logger logger, List<Player> players, Map<String, Double> preFlopOdds,
            int roundNumber, JLabel labelRound, List<JLabel> cardLabels,
            JLabel labelPot, JLabel labelAmountToCall)
    {
        this.logger = logger;
        this.players = players;
        this.preFlopOdds = preFlopOdds;
        this.roundNumber = roundNumber;
        this.deck = new Deck();
        this.cardsOnTable = new Hand(5);
        this.pot = 0;
        
        this.labelRound = labelRound;
        this.cardLabels = cardLabels;
        this.labelPot = labelPot;
        this.labelAmountToCall = labelAmountToCall;
        
        this.withGUI = (this.cardLabels != null);
    }
    
    /**
     * Play the round.
     */
    public void play()
    {
        this.logger.setRound(this.roundNumber);
        
        boolean roundIsOver = false,
                goToShowdown = false;
        int playersStillPlaying, amountOfWinners = 1;
        Queue<BettingRoundType> bettingRoundTypes = new LinkedList<>();
        bettingRoundTypes.addAll(Arrays.asList(BettingRoundType.values()));
        
        //initialize players for round
        for(Player p: this.players)
        {
            p.setForRoundStart();
        }
        this.gui_initializeLabels();
        
        while( !roundIsOver )
        {
            BettingRound br = new BettingRound(this.logger, this.players,
                    this.preFlopOdds, bettingRoundTypes.remove(), this.deck,
                    this.cardsOnTable, this.pot, this.cardLabels,
                    this.labelAmountToCall);
            
            //play betting round and add the resulting money to the pot
            this.pot += br.play();
            
            this.logStatus();   //write to log
            this.gui_updateLabels();
            
            //count players
            playersStillPlaying = 0;
            for(Player p: this.players)
            {
                if( !p.hasLostRound() )
                    playersStillPlaying += 1;
            }
            if(playersStillPlaying < 2)
                roundIsOver = true;
            else if(bettingRoundTypes.isEmpty())
                goToShowdown = true;
            
            if( !goToShowdown && !roundIsOver )
            {
                for(Player p: this.players)
                {
                    if(p.IsAllIn())
                        goToShowdown = true;
                }
            }
            
            if(goToShowdown)
            {
                amountOfWinners = this.showCards();
                roundIsOver = true;
            }
        }
        
        if( !goToShowdown )
        {
            //log cards; if showCards happend, then they have been logged already
            this.logger.print("table: " + this.cardsOnTable);
            for(Player p: this.players)
            {
                this.logger.print(String.format("%s: %s %s", p, p.getOddsToWin(),
                        p.getHand()));
            }
        }
        
        //give money to winner
        int winnersMoney = this.pot / amountOfWinners;
        for(Player p: this.players)
        {
            p.updateGUI(null);
            if( !p.hasLostRound() )
            {
                p.wonRound(winnersMoney);
                this.logger.print("Give " + winnersMoney + " to player " + p);
            }
        }
        
        this.logger.unsetRound();
    }
    
    /**
     * Calculate the winner.
     * @return the amount of winners
     */
    private int showCards()
    {
        List<Player> thePlayers = new LinkedList<>(),
                outPlayers = new LinkedList<>();
        List<Combination> theCombs = new LinkedList<>(),
                outCombs = new LinkedList<>();
        List<Hand> theHands = new LinkedList<>(),
                outHands = new LinkedList<>();
        
        Combination[] returnComb = new Combination[1];
        String[] returnCombDsc = new String[1];
        Hand[] returnHand = new Hand[1];
        
        //if one player is all in, there might not be 5 cards on the table yet
        while(this.cardsOnTable.getCardCount() < 5)
        {
            this.cardsOnTable.addCard( this.deck.dealCard() );
            this.deck.dealCard();       //discard one card
        }
        this.gui_showCards();
        this.logger.print("showCards table: " + this.cardsOnTable);
        
        //evaluate hands of players
        for(Player p: this.players)
        {
            if( !p.hasLostRound() )
            {
                returnComb[0] = null;
                returnCombDsc[0] = null;
                returnHand[0] = null;
                HandEvaluator.evaluate(this.cardsOnTable, p.getHand(),
                        returnComb, returnCombDsc, returnHand);
                
                thePlayers.add(p);
                theCombs.add(returnComb[0]);
                theHands.add(returnHand[0]);
                
                this.logger.print(String.format("showCards %s: %s %s %s",
                        p, p.getOddsToWin(), p.getHand(), returnComb[0]));
                p.gui_addToTextArea("Combination: " + returnComb[0]);
            }
        }
        
        //determine winner
        boolean[] posWinners = HandEvaluator.get_winners(
                thePlayers, theCombs, theHands,
                outPlayers, outCombs, outHands);
        
        int amountOfWinners = 0;
        for(int i=0; i < this.players.size(); i++)
        {
            if(posWinners[i])
                amountOfWinners++;
            else
                this.players.get(i).lostShowdown();
        }
        
        this.logger.print("showCards amountOfWinners=" + amountOfWinners);
        
        return amountOfWinners;
    }
    
    /**
     * Write to log.
     */
    private void logStatus()
    {
        //ONLY for 2 players
        Player p1, p2;
        if(this.players.get(0).toString().contains("2"))
        {
            p1 = this.players.get(1);
            p2 = this.players.get(0);
        }
        else
        {
            p1 = this.players.get(0);
            p2 = this.players.get(1);
        }
        this.logger.print(String.format("(pot = %5d) (%s = %5d) (%s = %5d)",
                this.pot, p1, p1.getMoney(), p2, p2.getMoney()));
    }
    
    /**
     * Initialize GUI.
     */
    private void gui_initializeLabels()
    {
        if(this.withGUI)
        {
            ImageIcon img = new ImageIcon(getClass().getResource(
                    "/resources/" + PokerGUI.BACK_OF_CARDS_IMAGE_FILE));
            for(JLabel l: this.cardLabels)
            {
                l.setIcon(img);
            }
            
            this.labelRound.setText("Round " + this.roundNumber);
            this.labelPot.setText(String.format("%,d (%.1f %%)",
                    this.pot,
                    ((double)this.pot * 100) / Game.TOTAL_MONEY));
            this.labelAmountToCall.setText("0 (0.0 %)");
        }
    }
    
    /**
     * Initialize GUI.
     */
    private void gui_showCards()
    {
        if(this.withGUI)
        {
            //show all cards on table
            //in case of all in, some cards may not be visible yet
            for(int i=0; i<5; i++)
            {
                this.cardLabels.get(i).setIcon( new ImageIcon(getClass()
                        .getResource("/resources/" + this.cardsOnTable
                                .getCard(i).getNameOfImageFile())) );
            }
        }
    }
    
    /**
     * Updates GUI.
     */
    private void gui_updateLabels()
    {
        if(this.withGUI)
        {
            this.labelPot.setText(String.format("%,d (%.1f %%)",
                    this.pot,
                    ((double)this.pot * 100) / Game.TOTAL_MONEY));
            this.labelAmountToCall.setText("0 (0.0 %)");
        }
    }
}
