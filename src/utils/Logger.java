package utils;

import enums.BettingRoundType;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A customized simple logger.
 * @author Ralf
 */
public final class Logger
{
	/**
	 * Attributes.
	 */
    private final DateFormat dateFormat;
	private PrintWriter writer;
    private boolean closeable;
    private int game, round, bettingRound;
    
	/**
	 * Constructor.
	 * @param fileName of the log file
	 */
	public Logger(String fileName)
	{
        this.dateFormat = new SimpleDateFormat("HH:mm:ss.SSSS");
		this.game = 0;
        this.round = 0;
        this.bettingRound = 0;
        
        if( !fileName.isEmpty() )
        {
            fileName = "Log_" + fileName + ".log";
            try
            {
                //create writer that appends if the file exists,
                //and with automatic flushing
                this.writer = new PrintWriter(
                        new FileOutputStream(fileName, true),
                        true);
                this.print("Opening log file.");
                this.closeable = true;
            }
            catch(FileNotFoundException e)
            {
                this.writer = new PrintWriter(System.out);
                this.print("Could not open log file: " + e);
                this.closeable = false;
            }
        }
        else
        {
            this.writer = new PrintWriter(System.out);
            this.print("Starting log to stdout.");
            this.closeable = false;
        }
	}
    
	/**
	 * Prints the msg into the log.
	 * @param msg to be logged 
	 */
	public void print(String msg)
	{
        String g = "", r = "", br = "";
        if(this.game > 0)
            g = String.format("g%02d", this.game);
        if(this.round > 0)
            r = String.format("r%03d", this.round);
        if(this.bettingRound > 0)
            br = String.format("br%1d", this.bettingRound);
        
        this.writer.println(String.format("%s > %3s %4s %3s >> %s",
                this.getDateTime(), g, r, br, msg));
	}
	
	/**
	 * Get current date and time.
	 */
	private String getDateTime()
	{
        return this.dateFormat.format( new Date() );
	}
	
    /**
     * Set game.
     * @param game 
     */
    public void setGame(int game)
    {
        this.game = game;
        this.print("Start");
    }
    
    /**
     * Unset game.
     * @param duration
     */
    public void unsetGame(String duration)
    {
        this.print("End; " + duration);
        this.game = 0;
    }
    
    /**
     * Set round.
     * @param round 
     */
    public void setRound(int round)
    {
        this.round = round;
        this.print("Start");
    }
    
    /**
     * Unset round.
     */
    public void unsetRound()
    {
        this.print("End");
        this.round = 0;
    }
    
    /**
     * Set betting round.
     * @param bettingRoundType
     */
    public void setBettingRound(BettingRoundType bettingRoundType)
    {
        switch(bettingRoundType)
        {
            case PRE_FLOP:
                this.bettingRound = 1;
                break;
            case FLOP:
                this.bettingRound = 2;
                break;
            case TURN:
                this.bettingRound = 3;
                break;
            default:
                this.bettingRound = 4;
                break;
        }
        this.print("Start");
    }
	
    /**
     * Unset betting round.
     * @param bettingRoundPot
     */
    public void unsetBettingRound(int bettingRoundPot)
    {
        this.print("End; bettingRoundPot = " + bettingRoundPot);
        this.bettingRound = 0;
    }
	
	/**
	 * Closes the log.
	 */
	public void close()
	{
		this.print("Closing logger.");
        if(this.closeable)
            this.writer.close();
        else
            this.writer.flush();
	}
}
