package concepts;

import enums.ActionType;
import enums.BettingRoundType;

/**
 * The AI strategy for a poker player.
 */
public class StrategyOne
{
    /**
     * AI.
     * @param odds
     * @param type
     * @param cardsOnTable
     * @param cardsOfPlayer
     * @param thisCase
     * @param qtyRaises
     * @return 
     */
    public static ActionType getRecomendedAction(Odds odds, BettingRoundType type,
            Hand cardsOnTable, Hand cardsOfPlayer, int thisCase, int qtyRaises)
    {
        ActionType at;              
        double prob = 1/Math.pow(2,qtyRaises);
        double k = Math.random();
        
        switch(type)
        {
            case PRE_FLOP:
                if (odds.getWins() < 25)
                    at = ActionType.FOLD;
                else if (odds.getWins() < 60)
                    at = ActionType.CHECK;
                else 
                {
                    if(k <= prob)
                        at = ActionType.RAISE;
                    else
                        at = ActionType.CHECK;
                }
                break;
            case FLOP:
                if (odds.getWins() < 35)
                    at = ActionType.FOLD;
                else if (odds.getWins() < 65)
                    at = ActionType.CHECK;
                else
                {
                    if(k <= prob)
                        at = ActionType.RAISE;
                    else
                        at = ActionType.CHECK;
                }
                break;
            case TURN:
                if (odds.getWins() < 45)
                    at = ActionType.FOLD;
                else if (odds.getWins() < 70)
                    at = ActionType.CHECK;
                else
                {
                    if(k <= prob)
                        at = ActionType.RAISE;
                    else
                        at = ActionType.CHECK;
                }
                break;
            default:
                if (odds.getWins() < 45)
                    at = ActionType.FOLD;
                else if (odds.getWins() < 75)
                    at = ActionType.CHECK;
                else
                {
                    if(k <= prob)
                        at = ActionType.RAISE;
                    else
                        at = ActionType.CHECK;
                }
        }
        
        switch(thisCase)
        {
            case 1:     //no current amount to call
                if(at == ActionType.FOLD)
                    at = ActionType.CHECK;
                break;
            case 2:     //amount to call is bigger than money
                if(at == ActionType.RAISE || at == ActionType.CHECK)
                    at = ActionType.CALL;
                //at = ActionType.FOLD;   //do not go all in
                break;
            case 3:     //has enough money to call, but not for a raise
                if(at == ActionType.RAISE || at == ActionType.CHECK)
                    at = ActionType.CALL;
                break;
            default:    //has enough money to call or raise
        }
        
        return at;
    }
}
