package concepts;

/**
 * An object of type Deck represents a deck of 52 playing cards.
 * @author Ralf
 */
public class Deck
{
    /**
     * An array of 52.
     */
    private final Card[] deck;
    
    /**
     * Keeps track of the number of cards that have been dealt from the deck.
     */
    private int cardsUsed;
    
    /**
     * Constructs a regular 52-card poker deck.
     * The shuffle() method is called inside the constructor, so the cards are
     * already in a random order.
     */
    public Deck()
    {
        this.deck = new Card[52];
        int cardCt = 0;     // How many cards have been created so far.
        for(int suit = 1; suit < 5; suit++)
        {
            for(int value = 2; value <= 14; value++)
            {
                this.deck[cardCt] = new Card(value, suit);
                cardCt++;
            }
        }
        this.cardsUsed = 0;
        this.shuffle();
    }
    
    /**
     * Create a deck minus the cards that are in the hand.
     * @param hand 
     */
    public Deck(Hand hand)
    {
        Card c;
        this.deck = new Card[52 - hand.getCardCount()];
        int cardCt = 0;     // How many cards have been created so far.
        for(int suit = 1; suit < 5; suit++)
        {
            for(int value = 2; value <= 14; value++)
            {
                c = new Card(value, suit);
                if( !hand.containsCard(c) )
                {
                    this.deck[cardCt] = c;
                    cardCt++;
                }
            }
        }
        this.cardsUsed = 0;
    }
    
    /**
     * Put all the used cards back into the deck (if any), and shuffle.
     */
    public final void shuffle()
    {
        int rand;
        for(int i = this.deck.length-1; i > 0; i--)
        {
            rand = (int)(Math.random()*(i+1));
            Card temp = this.deck[i];
            this.deck[i] = this.deck[rand];
            this.deck[rand] = temp;
        }
        this.cardsUsed = 0;
    }
    
    /**
     * Returns the number of cards that are still left in the deck.
     * @return
     */
    public int cardsLeft()
    {
        return this.deck.length - this.cardsUsed;
    }
    
    /**
     * Removes the next card from the deck and returns it.
     * It is illegal to call this method if there are no more cards in the deck.
     * You can check the number of cards remaining by calling the cardsLeft()
     * method.
     * @return the card which is removed from the deck.
     */
    public Card dealCard()
    {
        if(this.cardsUsed == this.deck.length)
        {
            throw new RuntimeException("No cards are left in the deck ");
        }
        this.cardsUsed++;
        return this.deck[this.cardsUsed - 1];
        
        /* Programming note:  Cards are not literally removed from the array
         * that represents the deck.  We just keep track of how many cards
         * have been used.
         */
    }
    
}
