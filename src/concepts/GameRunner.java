package concepts;

import gui.PokerGUI;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import utils.Logger;

/**
 * Play.
 */
public class GameRunner
{
    /**
     * Start a game.
     * @param args
     */
    public static void main(String[] args)
    {
        if(args.length >= 1 && args[0].startsWith("1"))
        {
            GameRunner.runOneGame(args);
        }
        else if(args.length >= 1 && args[0].toUpperCase().startsWith("M"))
        {
            GameRunner.runMultipleGames(args);
        }
        else
        {
            //play with GUI
            System.out.println("Starting GUI");
            PokerGUI.main(null);
        }
    }
    
    /**
     * Run one game.
     * Will print to standart output.
     * @param args 
     *      args[1]: rounds to play
     *      args[2]: strategy of player 1
     *      args[3]: strategy of player 2
     *      args[4]: q of player 1
     *      args[5]: q of player 2
     */
    public static void runOneGame(String[] args)
    {
        List<Integer> strategies = new LinkedList<>();
        strategies.add(Player.STRATEGY_1);
        strategies.add(Player.STRATEGY_2);
        strategies.add(Player.STRATEGY_3);
        
        int defaultStrategy = Player.STRATEGY_1,
                strategy1 = defaultStrategy,
                strategy2 = defaultStrategy,
                defaultQtyRounds = 200,
                qtyRounds = defaultQtyRounds;
        double defaultQ = 0.5,
                q1 = defaultQ,
                q2 = defaultQ;
        
        if(args.length >= 2)
        {
            //get qty of rounds to play
            try
            {
                qtyRounds = Integer.parseInt(args[1]);
            }
            catch(NumberFormatException ex) {}
        }
        
        if(args.length >= 4)
        {
            //get strategy for P1
            try
            {
                strategy1 = Integer.parseInt(args[2]);
                if( !strategies.contains(strategy1) )
                    strategy1 = defaultStrategy;
            }
            catch(NumberFormatException ex) {}
            
            //get strategy for P2
            try
            {
                strategy2 = Integer.parseInt(args[3]);
                if( !strategies.contains(strategy2) )
                    strategy2 = defaultStrategy;
            }
            catch(NumberFormatException ex) {}
        }
        
        if(args.length >= 5 && strategy1 == Player.STRATEGY_1)
        {
            //get q for P1
            try
            {
                q1 = Double.parseDouble(args[4]);
                if( !(0 <= q1 && q1 <= 1) )
                    q1 = defaultQ;
            }
            catch(NumberFormatException ex) {}
        }
        
        if(args.length >= 6 && strategy2 == Player.STRATEGY_1)
        {
            //get q for P2
            try
            {
                q2 = Double.parseDouble(args[5]);
                if( !(0 <= q2 && q2 <= 1) )
                    q2 = defaultQ;
            }
            catch(NumberFormatException ex) {}
        }
        
        System.out.println(String.format("Starting game (rounds = %d) "
                + "(str1 = %d) (str2 = %d) (q1 = %3.1f) (q2 = %3.1f)",
                qtyRounds, strategy1, strategy2, q1, q2));
        
        Game g = new Game(
                new Player("p1", strategy1, q1),
                new Player("p2", strategy2, q2) );
        String[] results = g.play(qtyRounds);
        
        for(String s: results)
            System.out.println(String.format("   %s", s));
    }
    
    /**
     * Run many games.
     * Will create a log file for each game.
     * @param args 
     */
    public static void runMultipleGames(String[] args)
    {
        Map<String, Double> mapQs = new TreeMap<>();
        List<Player> listPlayers1 = new LinkedList<>();
        Player p1, p2;
        String[] results;
        String s;
        double q;
        int i, j;
        long timeDiff, timeEnd, timeStart = System.currentTimeMillis();
        
        //read Qs from args and parse them
        //convert them to only use 2 decimal positions
        for(i = 1; i < args.length; i++)
        {
            try
            {
                q = Double.parseDouble(args[i]);
                if(q > 1)
                    q = 1;
                else if(q < 0)
                    q = 0;
                
                j = new Double(q * 100).intValue();
                if(j == 100)
                    s = "p_1";
                else if(j == 0)
                    s = "p_0";
                else
                    s = String.format("p%d", j);
                mapQs.put(s, (double)j/100);
            }
            catch(NumberFormatException ex) {}
        }
        
        //create players
        s = "";
        for(Map.Entry<String, Double> e: mapQs.entrySet())
        {
            s += ", " + e.getKey();
            listPlayers1.add( new Player(e.getKey(), Player.STRATEGY_1, e.getValue()) );
        }
        System.out.println(String.format("%d players: %s", listPlayers1.size(),
                s.substring(2)));
        System.out.println();
        
        //run games between each pair of players
        for(i = 0; i < listPlayers1.size(); i++)
        {
            for(j = i+1; j < listPlayers1.size(); j++)
            {
                p1 = listPlayers1.get(i);
                p2 = listPlayers1.get(j);
                
                System.out.println(String.format("Start game %s vs %s", p1, p2));
                
                Game g = new Game(new Logger(String.format("%s_vs_%s", p1, p2)),
                        p1, p2);
                results = g.play(200);
                
                for(String s1: results)
                    System.out.println(String.format("   %s", s1));
            }
        }
        timeEnd = System.currentTimeMillis();
        timeDiff = timeEnd - timeStart;
        
        //print totals
        Collections.sort(listPlayers1, new Comparator<Player>() {
            @Override
            public int compare(Player p1, Player p2)
            {
                if(p1.getTotalGamesWon() < p2.getTotalGamesWon())
                    return 1;
                else if(p1.getTotalGamesWon() > p2.getTotalGamesWon())
                    return -1;
                else
                {
                    if(p1.getTotalRoundsWon() < p2.getTotalRoundsWon())
                        return 1;
                    else if(p1.getTotalRoundsWon() > p2.getTotalRoundsWon())
                        return -1;
                    else
                    {
                        return 0;
                    }
                }
            }
        });
        System.out.println();
        System.out.println("##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####");
        System.out.println("##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####");
        System.out.println(String.format("Duration: %,d ms = %.1f min",
                timeDiff, (double)timeDiff / 60000));
        System.out.println("Final ranking of players:");
        System.out.println(String.format("   %-6s | %-10s | %-14s | %-9s | %-9s",
                "Player", "Games won", "Rounds won", "Money won", "Money lost"));
        for(Player p: listPlayers1)
        {
            System.out.println(String.format("   %-6s | %,3d of %,3d | %,5d of %,5d "
                    + "| %,9d | %,9d", p,
                    p.getTotalGamesWon(), p.getTotalGamesPlayed(),
                    p.getTotalRoundsWon(), p.getTotalRoundsPlayed(),
                    p.getTotalMoneyWon(), p.getTotalMoneyLost()));
        }
        System.out.println("##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####");
        System.out.println("##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####");
    }
}
