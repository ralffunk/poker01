package enums;

/**
 * All action types.
 * @author Ralf
 */
public enum ActionType
{
    CHECK,
    RAISE,
    CALL,
    FOLD;
}
