package concepts;

/**
 * An object of type Card represents a playing card from a standard Poker deck.
 * The card has a suit, which can be spades, hearts, diamonds or clubs.
 * Each card has one of the 13 ranks: 2, 3, 4, 5, 6, 7, 8, 9, 10, jack, 
 * queen, king or ace. Note that "ace" is considered to be the highest rank.
 * @author Ralf
 */
public class Card
{
    /**
     * Class constants.
     */
    public final static int JACK = 11,
            QUEEN = 12,
            KING = 13,
            ACE = 14;
    public final static int SPADES = 4,
            HEARTS = 3,
            DIAMONDS = 2,
            CLUBS = 1;
    
    /**
     * The card's rank, one of the values 2 through 14, with 14 representing
     * the ACE.
     */
    private final int rank;
    
    /**
     * This card's suit, one of the constants SPADES, HEARTS, DIAMONDS or
     * CLUBS. The suit cannot be changed after the card is constructed.
     */
    private final int suit; 
    
    /**
     * Creates a card with a specified suit and rank.
     * @param theRank 
     * @param theSuit 
     * @throws IllegalArgumentException 
     */
    public Card(int theRank, int theSuit)
    {
        if(theRank < 2 || theRank > 14)
        {
            throw new IllegalArgumentException("Illegal rank: " + theRank);
        }
        if(theSuit != SPADES && theSuit != HEARTS && 
                theSuit != DIAMONDS && theSuit != CLUBS)
        {
            throw new IllegalArgumentException("Illegal suit: " + theSuit);
        }
        this.rank = theRank;
        this.suit = theSuit;
    }
    
    /**
     * Returns the rank of this card.
     * @return 
     */
    public int getRank()
    {
        return this.rank;
    }
    
    /**
     * Returns the suit of this card.
     * @return 
     */
    public int getSuit()
    {
        return this.suit;
    }
    
    /**
     * Returns a String representation of the card's rank.
     * @return one of the strings "Ace", "2", ... "Queen", or "King".
     */
    public String getRankAsString()
    {
        switch(this.rank)
        {
            case 2:   return "2";
            case 3:   return "3";
            case 4:   return "4";
            case 5:   return "5";
            case 6:   return "6";
            case 7:   return "7";
            case 8:   return "8";
            case 9:   return "9";
            case 10:  return "10";
            case 11:  return "Jack";
            case 12:  return "Queen";
            case 13:  return "King";
            default:  return "Ace";
        }
    }
    
    /**
     * Returns a String representation of the card's suit.
     * @return one of the strings "Spades", "Hearts", "Diamonds" or "Clubs".
     */
    public String getSuitAsString()
    {
        switch(this.suit)
        {
            case SPADES:   return "Spades";
            case HEARTS:   return "Hearts";
            case DIAMONDS: return "Diamonds";
            default:       return "Clubs";
        }
    }
    
    /**
     * Returns a string representation of this card, including both
     * its suit and its rank.
     * @return Sample return values are: "Queen of Hearts",
     * "10 of Diamonds", "Ace of Spades".
     */
    @Override
    public String toString()
    {
        return this.getRankAsString() + " of " + this.getSuitAsString();
    }
    
    /**
     * Returns the name of the image file that goes with this card.
     * This method is used for the GUI.
     * @return a String, for example "s2.png" or "c13.png".
     */
    public String getNameOfImageFile()
    {
        String name;
        switch(this.suit)
        {
            case SPADES:
                name = "s";
                break;
            case HEARTS:
                name = "h";
                break;
            case DIAMONDS:
                name = "d";
                break;
            default:
                name = "c";
        }
        return name + this.rank + ".png";
    }
    
}
