package concepts;

import enums.ActionType;
import enums.BettingRoundType;

/**
 * The AI strategy for a poker player.
 */
public class StrategyTwo
{
    /**
     * AI.
     * @param type
     * @param cardsOnTable
     * @param cardsOfPlayer
     * @param thisCase
     * @return 
     */
    public static ActionType getRecomendedAction(BettingRoundType type,
            Hand cardsOnTable, Hand cardsOfPlayer, int thisCase)
    {
        ActionType at;
        
        double k = Math.random();
        
        switch(thisCase)
        {
            case 1:     //no current amount to call
                if(k < 0.6)
                    at = ActionType.CHECK;
                else
                    at = ActionType.RAISE;
                break;
            case 2:     //amount to call is bigger than money
                at = ActionType.FOLD;   //do not go all in
                break;
            case 3:     //has enough money to call, but not for a raise
                if(k < 0.5)
                    at = ActionType.CALL;
                else
                    at = ActionType.FOLD;
                break;
            default:    //has enough money to call or raise
                if(k < 0.4)
                    at = ActionType.CALL;
                else if(k < 0.7)
                    at = ActionType.RAISE;
                else
                    at = ActionType.FOLD;
                break;
        }
        
        return at;
    }
}
