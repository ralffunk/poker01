package concepts;

/**
 * Used by methods in OddsCalculator class to return data from evaluation of odds.
 */
public class Odds
{
    /**
     * Attributes.
     */
    private final double wins;
    private final double losses;
    private final double ties;
    private final int nodes;
    
    /**
     * Constructor.
     * @param wins
     * @param losses
     * @param ties
     * @param nodes 
     */
    public Odds(double wins, double losses, double ties, int nodes)
    {
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.nodes = nodes;
    }
    
    public double getWins()
    {
        return wins;
    }

    public double getLosses()
    {
        return losses;
    }

    public double getTies()
    {
        return ties;
    }

    public int getNodes()
    {
        return nodes;
    }
    
}
