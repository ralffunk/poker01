package tests;

import concepts.Card;
import concepts.Hand;
import concepts.HandEvaluator;
import concepts.Player;
import enums.Combination;
import java.util.LinkedList;
import java.util.List;
import org.junit.Test;
import org.junit.Assert;

/**
 * Test the evaluator of hands.
 * @author Ralf
 */
public class HandEvaluatorTest
{
    @Test
    public void testEvaluation()
    {
        //list of test cases
        int[][][] cases = {
            //Combination.STRAIGHT_FLUSH
            {{2, 1}, {3, 1}, {4, 1}, {5, 1}, {6, 1}, {7, 2}, {8, 1}},
            {{7, 1}, {3, 1}, {4, 1}, {5, 1}, {6, 1}, {7, 2}, {8, 1}},
            {{2, 1}, {3, 1}, {4, 1}, {5, 1}, {6, 1}, {7, 1}, {9, 1}},
            //Combination.POKER
            {{2, 1}, {2, 2}, {2, 3}, {2, 4}, {6, 1}, {7, 1}, {9, 1}},
            {{2, 1}, {2, 2}, {2, 3}, {2, 4}, {14, 4}, {14, 1}, {14, 3}},
            {{2, 1}, {2, 2}, {2, 3}, {2, 4}, {3, 1}, {6, 2}, {5, 1}},
            //Combination.FULL_HOUSE
            {{2, 1}, {2, 3}, {2, 4}, {5, 1}, {6, 1}, {7, 1}, {7, 2}},
            {{9, 2}, {9, 1}, {4, 1}, {5, 1}, {14, 1}, {14, 3}, {14, 2}},
            {{2, 1}, {3, 1}, {4, 1}, {3, 4}, {6, 1}, {2, 4}, {2, 2}},
            //Combination.FLUSH
            {{2, 1}, {3, 1}, {4, 1}, {5, 1}, {7, 1}, {8, 1}, {9, 1}},
            {{2, 1}, {3, 1}, {4, 1}, {5, 1}, {6, 2}, {7, 1}, {8, 1}},
            {{2, 1}, {3, 3}, {4, 1}, {5, 1}, {6, 1}, {7, 2}, {14, 1}},
            //Combination.STRAIGHT
            {{3, 2}, {3, 1}, {4, 1}, {5, 1}, {6, 1}, {7, 3}, {8, 2}},
            {{3, 4}, {3, 2}, {4, 4}, {5, 1}, {6, 1}, {7, 1}, {7, 4}},
            {{2, 1}, {3, 3}, {4, 4}, {5, 1}, {6, 1}, {6, 4}, {6, 2}},
            //Combination.TRIO
            {{2, 2}, {3, 1}, {4, 1}, {3, 3}, {6, 1}, {7, 1}, {3, 4}},
            {{2, 1}, {3, 1}, {4, 1}, {5, 1}, {14, 3}, {14, 4}, {14, 2}},
            {{4, 4}, {4, 3}, {4, 2}, {5, 1}, {6, 1}, {7, 1}, {9, 1}},
            //Combination.TWO_PAIRS
            {{2, 1}, {2, 2}, {4, 1}, {4, 2}, {6, 1}, {7, 1}, {8, 2}},
            {{2, 1}, {3, 1}, {3, 4}, {5, 2}, {6, 1}, {8, 4}, {8, 1}},
            {{6, 4}, {6, 2}, {7, 1}, {9, 2}, {9, 1}, {10, 2}, {10, 1}},
            //Combination.ONE_PAIR
            {{2, 1}, {3, 3}, {4, 4}, {4, 2}, {6, 1}, {7, 1}, {8, 1}},
            {{13, 4}, {3, 1}, {4, 2}, {5, 1}, {6, 1}, {14, 2}, {14, 1}},
            {{2, 1}, {2, 2}, {4, 1}, {8, 1}, {9, 1}, {10, 3}, {11, 4}},
            //Combination.H_CARD
            {{2, 1}, {7, 1}, {4, 2}, {5, 1}, {6, 1}, {13, 2}, {14, 2}},
            {{4, 1}, {5, 1}, {6, 1}, {8, 2}, {9, 2}, {10, 2}, {11, 2}},
        };
        Combination[] expectedCombinations = {
            Combination.STRAIGHT_FLUSH,
            Combination.STRAIGHT_FLUSH,
            Combination.STRAIGHT_FLUSH,
            Combination.POKER,
            Combination.POKER,
            Combination.POKER,
            Combination.FULL_HOUSE,
            Combination.FULL_HOUSE,
            Combination.FULL_HOUSE,
            Combination.FLUSH,
            Combination.FLUSH,
            Combination.FLUSH,
            Combination.STRAIGHT,
            Combination.STRAIGHT,
            Combination.STRAIGHT,
            Combination.TRIO,
            Combination.TRIO,
            Combination.TRIO,
            Combination.TWO_PAIRS,
            Combination.TWO_PAIRS,
            Combination.TWO_PAIRS,
            Combination.ONE_PAIR,
            Combination.ONE_PAIR,
            Combination.ONE_PAIR,
            Combination.H_CARD,
            Combination.H_CARD,
        };
        int[][] expectedOrderOfHands = {
            //Combination.STRAIGHT_FLUSH
            {0, 1, 2, 3, 4},
            {2, 3, 4, 0, 6},
            {1, 2, 3, 4, 5},
            //Combination.POKER
            {6, 0, 1, 2, 3},
            {4, 0, 1, 2, 3},
            {5, 0, 1, 2, 3},
            //Combination.FULL_HOUSE
            {5, 6, 0, 1, 2},
            {1, 0, 4, 6, 5},
            {1, 3, 0, 6, 5},
            //Combination.FLUSH
            {2, 3, 4, 5, 6},
            {1, 2, 3, 5, 6},
            {0, 2, 3, 4, 6},
            //Combination.STRAIGHT
            {2, 3, 4, 5, 6},
            {0, 2, 3, 4, 6},
            {0, 1, 2, 3, 5},
            //Combination.TRIO
            {4, 5, 1, 3, 6},
            {2, 3, 6, 4, 5},
            {5, 6, 2, 1, 0},
            //Combination.TWO_PAIRS
            {6, 0, 1, 2, 3},
            {4, 1, 2, 6, 5},
            {2, 4, 3, 6, 5},
            //Combination.ONE_PAIR
            {4, 5, 6, 3, 2},
            {3, 4, 0, 6, 5},
            {4, 5, 6, 0, 1},
            //Combination.H_CARD
            {3, 4, 1, 5, 6},
            {2, 3, 4, 5, 6},
        };
        
        Hand table = new Hand(5),
                player = new Hand(2);
        Combination[] returnComb = new Combination[1];
        String[] returnCombDsc = new String[1];
        Hand[] returnHand = new Hand[1];
        int i, j;
        int[][] c;
        Card card;
        
        for(i=0; i < cases.length; i++)
        {
            c = cases[i];
            
            table.clear();
            for(j=0; j<5; j++)
                table.addCard( new Card(c[j][0], c[j][1]) );
            
            player.clear();
            for(j=5; j<7; j++)
                player.addCard( new Card(c[j][0], c[j][1]) );
            
            returnComb[0] = null;
            returnCombDsc[0] = null;
            returnHand[0] = null;
            HandEvaluator.evaluate(table, player, returnComb, returnCombDsc,
                    returnHand);
            
            Assert.assertEquals(String.format("i=%d (combination)", i),
                    expectedCombinations[i],
                    returnComb[0]);
            Assert.assertEquals(String.format("i=%d (combination description)", i),
                    returnComb[0].toString(),
                    returnCombDsc[0]);
            Assert.assertEquals(String.format("i=%d (winningHand.lenght)", i),
                    5,
                    returnHand[0].getCardCount());
            
            for(j=0; j<5; j++)
            {
                card = returnHand[0].getCard(j);
                Assert.assertEquals(String.format("i=%d, j=%d (rank)", i, j),
                        c[ expectedOrderOfHands[i][j] ][0],
                        card.getRank());
                Assert.assertEquals(String.format("i=%d, j=%d (suit)", i, j),
                        c[ expectedOrderOfHands[i][j] ][1],
                        card.getSuit());
            }
        }
    }
    
    @Test
    public void testDeterminationOfWinners()
    {
        //list of test cases
        int[][][] cases = {
            //Combination.STRAIGHT_FLUSH
            {
                {14, 1}, {3, 2}, {4, 2}, {5, 2}, {6, 2},
                {7, 2}, {8, 1},
                {2, 2}, {14, 3},
                {10, 1}, {11, 3},
            },
            {
                {2, 2}, {3, 2}, {4, 2}, {5, 2}, {6, 2},
                {8, 2}, {8, 1},
                {9, 2}, {14, 3},
                {10, 1}, {11, 3},
            },
            //Combination.POKER
            {
                {3, 2}, {5, 2}, {5, 1}, {6, 2}, {6, 1},
                {5, 3}, {5, 4},
                {6, 3}, {6, 4},
                {10, 1}, {11, 3},
            },
            {
                {3, 2}, {5, 2}, {5, 1}, {6, 2}, {6, 1},
                {6, 3}, {6, 4},
                {5, 3}, {5, 4},
                {10, 2}, {11, 2},
            },
            //Combination.FULL_HOUSE
            {
                {3, 2}, {5, 2}, {5, 1}, {6, 2}, {8, 1},
                {5, 3}, {6, 3},
                {5, 4}, {8, 4},
                {10, 1}, {11, 3},
            },
            {
                {3, 2}, {5, 2}, {5, 1}, {6, 2}, {8, 1},
                {5, 3}, {8, 3},
                {5, 4}, {8, 4},
                {10, 1}, {11, 3},
            },
            //Combination.FLUSH
            {
                {4, 2}, {5, 2}, {6, 2}, {7, 2}, {12, 1},
                {2, 2}, {14, 1},
                {9, 2}, {14, 3},
                {10, 1}, {11, 3},
            },
            {
                {4, 2}, {5, 2}, {6, 4}, {12, 2}, {13, 2},
                {3, 2}, {14, 1},
                {2, 2}, {14, 3},
                {10, 1}, {11, 3},
            },
            //Combination.STRAIGHT
            {
                {14, 1}, {3, 2}, {4, 4}, {5, 4}, {6, 2},
                {7, 2}, {8, 1},
                {2, 2}, {14, 3},
                {10, 1}, {11, 3},
            },
            {
                {2, 2}, {7, 2}, {4, 4}, {5, 4}, {6, 2},
                {8, 2}, {8, 3},
                {8, 1}, {14, 3},
                {10, 1}, {11, 3},
            },
            //Combination.TRIO
            {
                {3, 2}, {5, 2}, {5, 1}, {6, 2}, {8, 1},
                {5, 3}, {7, 3},
                {5, 4}, {2, 4},
                {10, 1}, {11, 3},
            },
            {
                {3, 2}, {5, 2}, {5, 1}, {6, 2}, {9, 1},
                {5, 3}, {8, 3},
                {5, 4}, {8, 4},
                {10, 1}, {11, 3},
            },
            //Combination.TWO_PAIRS
            {
                {3, 2}, {5, 2}, {14, 1}, {6, 2}, {8, 1},
                {5, 3}, {14, 3},
                {5, 4}, {8, 4},
                {10, 1}, {11, 3},
            },
            {
                {3, 2}, {5, 2}, {14, 1}, {6, 2}, {8, 1},
                {5, 3}, {8, 3},
                {5, 4}, {8, 4},
                {10, 1}, {11, 3},
            },
            //Combination.ONE_PAIR
            {
                {3, 2}, {4, 2}, {9, 1}, {6, 2}, {8, 1},
                {5, 4}, {8, 4},
                {2, 3}, {2, 2},
                {10, 1}, {11, 3},
            },
            {
                {5, 3}, {6, 2}, {7, 2}, {8, 2}, {14, 1},
                {2, 3}, {2, 4},
                {2, 1}, {2, 2},
                {10, 1}, {11, 3},
            },
            //Combination.H_CARD
            {
                {3, 2}, {5, 2}, {6, 1}, {7, 2}, {8, 1},
                {10, 3}, {2, 3},
                {10, 4}, {2, 4},
                {10, 1}, {11, 3},
            },
            {
                {6, 2}, {7, 2}, {8, 1}, {10, 2}, {11, 1},
                {2, 3}, {3, 3},
                {2, 4}, {3, 4},
                {3, 1}, {4, 3},
            },
        };
        Combination[][] expectedCombs = {
            {
                Combination.STRAIGHT_FLUSH,
                Combination.STRAIGHT_FLUSH,
                Combination.H_CARD,
            },
            {
                Combination.STRAIGHT_FLUSH,
                Combination.STRAIGHT_FLUSH,
                Combination.STRAIGHT_FLUSH,
            },
            {
                Combination.POKER,
                Combination.POKER,
                Combination.TWO_PAIRS,
            },
            {
                Combination.POKER,
                Combination.POKER,
                Combination.FLUSH,
            },
            {
                Combination.FULL_HOUSE,
                Combination.FULL_HOUSE,
                Combination.ONE_PAIR,
            },
            {
                Combination.FULL_HOUSE,
                Combination.FULL_HOUSE,
                Combination.ONE_PAIR,
            },
            {
                Combination.FLUSH,
                Combination.FLUSH,
                Combination.H_CARD,
            },
            {
                Combination.FLUSH,
                Combination.FLUSH,
                Combination.H_CARD,
            },
            {
                Combination.STRAIGHT,
                Combination.STRAIGHT,
                Combination.H_CARD,
            },
            {
                Combination.STRAIGHT,
                Combination.STRAIGHT,
                Combination.H_CARD,
            },
            {
                Combination.TRIO,
                Combination.TRIO,
                Combination.ONE_PAIR,
            },
            {
                Combination.TRIO,
                Combination.TRIO,
                Combination.ONE_PAIR,
            },
            {
                Combination.TWO_PAIRS,
                Combination.TWO_PAIRS,
                Combination.H_CARD,
            },
            {
                Combination.TWO_PAIRS,
                Combination.TWO_PAIRS,
                Combination.H_CARD,
            },
            {
                Combination.ONE_PAIR,
                Combination.ONE_PAIR,
                Combination.H_CARD,
            },
            {
                Combination.ONE_PAIR,
                Combination.ONE_PAIR,
                Combination.H_CARD,
            },
            {
                Combination.H_CARD,
                Combination.H_CARD,
                Combination.H_CARD,
            },
            {
                Combination.H_CARD,
                Combination.H_CARD,
                Combination.H_CARD,
            },
        };
        int[][] expectedWinners = {
            //Combination.STRAIGHT_FLUSH
            {0, },
            {0, 1, 2},
            //Combination.POKER
            {1, },
            {0, },
            //Combination.FULL_HOUSE
            {1, },
            {0, 1},
            //Combination.FLUSH
            {1, },
            {0, },
            //Combination.STRAIGHT
            {0, },
            {0, 1},
            //Combination.TRIO
            {0, },
            {0, 1},
            //Combination.TWO_PAIRS
            {0, },
            {0, 1},
            //Combination.ONE_PAIR
            {0, },
            {0, 1},
            //Combination.H_CARD
            {2, },
            {0, 1, 2},
        };
        
        List<Player> players = new LinkedList<>(),
                outPlayers = new LinkedList<>();
        List<Combination> combs = new LinkedList<>(),
                outCombs = new LinkedList<>();
        List<Hand> hands = new LinkedList<>(),
                outHands = new LinkedList<>();
        
        Hand table = new Hand(5),
                h = new Hand(2);
        Combination[] returnComb = new Combination[1];
        String[] returnCombDsc = new String[1];
        Hand[] returnHand = new Hand[1];
        int i, j, k, n;
        int[][] c;
        boolean[] posWinners;
        
        for(i=0; i < cases.length; i++)
        {
            c = cases[i];
            
            //cards of table
            table.clear();
            for(j=0; j<5; j++)
                table.addCard( new Card(c[j][0], c[j][1]) );
            
            players.clear();
            combs.clear();
            hands.clear();
            
            for(j=0; j<3; j++)
            {
                h.clear();
                for(k=0; k<2; k++)
                {
                    n = 5 + 2*j + k;
                    h.addCard( new Card(c[n][0], c[n][1]) );
                }
                
                returnComb[0] = null;
                returnCombDsc[0] = null;
                returnHand[0] = null;
                HandEvaluator.evaluate(table, h, returnComb, returnCombDsc,
                        returnHand);
                
                Assert.assertEquals(String.format("i=%d, j=%d (expectedCombs)", i, j),
                        expectedCombs[i][j],
                        returnComb[0]);
                
                players.add( new Player("" + j, Player.STRATEGY_1) );
                combs.add(returnComb[0]);
                hands.add(returnHand[0]);
            }
            
            posWinners = HandEvaluator.get_winners(players, combs, hands,
                    outPlayers, outCombs, outHands);
            
            Assert.assertEquals(String.format("i=%d (expectedWinners.length)", i),
                    expectedWinners[i].length,
                    outPlayers.size());
            
            for(j=0; j < expectedWinners[i].length; j++)
            {
                Assert.assertEquals(String.format("i=%d, j=%d (expectedWinners)", i, j),
                        String.valueOf(expectedWinners[i][j]),
                        outPlayers.get(j).toString());
            }
        }
    }
}
