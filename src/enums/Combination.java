package enums;

/**
 * The combinations of a hand.
 * @author Ralf
 */
public enum Combination
{
    H_CARD,
    ONE_PAIR,
    TWO_PAIRS,
    TRIO,
    STRAIGHT,
    FLUSH,
    FULL_HOUSE,
    POKER,
    STRAIGHT_FLUSH;
    
    @Override
    public String toString()
    {
        if(this == STRAIGHT_FLUSH)
            return "straight flush";
        else if(this == POKER)
            return "poker";
        else if(this == FULL_HOUSE)
            return "full house";
        else if(this == FLUSH)
            return "flush";
        else if(this == STRAIGHT)
            return "straight";
        else if(this == TRIO)
            return "trio";
        else if(this == TWO_PAIRS)
            return "two pairs";
        else if(this == ONE_PAIR)
            return "one pair";
        else
            return "highest card";
    }
}
