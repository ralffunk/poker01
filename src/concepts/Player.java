package concepts;

import enums.BettingRoundType;
import enums.ActionType;
import gui.NextRoundDialog;
import gui.PokerGUI;
import gui.UsersChoiceDialog;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextArea;

/**
 * A player in a poker game.
 * @author Ralf
 */
public final class Player
{
    /**
     * Class constants.
     */
    public static final int IS_USER = 0,
            STRATEGY_1 = 1,
            STRATEGY_2 = 2,
            STRATEGY_3 = 3;
    
    /**
     * Attributes.
     */
    private final String name;
    private final int strategy;
    private final double q;
    private final Hand hand;
    private int moneyInPocket,
            moneyOnTable,
            roundsWon,
            roundsLost,
            gameMoneyWon,
            gameMoneyLost,
            moneyAtStartOfRound,
            totalRoundsPlayed,
            totalRoundsWon,
            totalRoundsLost,
            totalGamesPlayed,
            totalGamesWon,
            totalGamesLost,
            totalMoneyWon,
            totalMoneyLost;
    private boolean hasBeenAskedInBettingRound,
            hasFolded,
            lostInShowdown,
            isAllIn,
            hasLeftGame;
    private double roundQ;
    private Map<BettingRoundType, Odds> roundOdds;
    private final boolean withGUI;
    private final PokerGUI parentFrame;
    private final JLabel labelName,
            labelCard1,
            labelCard2,
            labelMoney,
            labelBet;
    private final JTextArea textArea1,
            textArea2;
    
    /**
     * Constructor.
     * @param name
     * @param strategy
     */
    public Player(String name, int strategy)
    {
        this(name, strategy, 0.5, null, null, null, null, null, null, null, null);
    }
    
    /**
     * Constructor.
     * @param name
     * @param strategy
     * @param q
     */
    public Player(String name, int strategy, double q)
    {
        this(name, strategy, q, null, null, null, null, null, null, null, null);
    }
    
    /**
     * Constructor.
     * @param name
     * @param strategy
     * @param q
     * @param parentFrame
     * @param labelName
     * @param labelCard1
     * @param labelCard2
     * @param labelMoney
     * @param labelBet
     * @param textArea1
     * @param textArea2 
     */
    public Player(String name, int strategy, double q,
            PokerGUI parentFrame, JLabel labelName, JLabel labelCard1,
            JLabel labelCard2, JLabel labelMoney, JLabel labelBet,
            JTextArea textArea1, JTextArea textArea2)
    {
        this.name = name;
        this.strategy = strategy;
        this.q = q;
        this.hand = new Hand(2);    //hand with cardlimit 2
        
        this.moneyInPocket = 0;
        this.moneyOnTable = 0;
        this.roundsWon = 0;
        this.roundsLost = 0;
        this.gameMoneyWon = 0;
        this.gameMoneyLost = 0;
        this.moneyAtStartOfRound = 0;
        this.totalRoundsPlayed = 0;
        this.totalRoundsWon = 0;
        this.totalRoundsLost = 0;
        this.totalGamesPlayed = 0;
        this.totalGamesWon = 0;
        this.totalGamesLost = 0;
        this.totalMoneyWon = 0;
        this.totalMoneyLost = 0;
        
        this.hasBeenAskedInBettingRound = false;
        this.hasFolded = false;
        this.lostInShowdown = false;
        this.isAllIn = false;
        this.hasLeftGame = false;
        
        this.roundQ = 0.0;
        this.roundOdds = new LinkedHashMap<>();
        
        this.parentFrame = parentFrame;
        this.labelName = labelName;
        this.labelCard1 = labelCard1;
        this.labelCard2 = labelCard2;
        this.labelMoney = labelMoney;
        this.labelBet = labelBet;
        this.textArea1 = textArea1;
        this.textArea2 = textArea2;
        
        this.withGUI = (this.parentFrame != null);
        
        this.setForGameStart(0);
    }
    
    /**
     * Set player for a new game.
     * @param money 
     */
    public void setForGameStart(int money)
    {
        this.moneyInPocket = money;
        this.moneyOnTable = 0;
        this.roundsWon = 0;
        this.roundsLost = 0;
        this.gameMoneyWon = 0;
        this.gameMoneyLost = 0;
        this.totalGamesPlayed++;
        this.hasLeftGame = false;
        
        this.gui_updateLabelName();
        this.gui_updateLabelCards(0);
        this.gui_updateLabelMoney();
        this.gui_setTextAreaColor(1, 0);
        this.gui_setTextAreaColor(2, 0);
        this.gui_setTextArea(1, "");
        this.gui_setTextArea(2, "");
    }
    
    /**
     * Set player for a new round.
     */
    public void setForRoundStart()
    {
        this.hand.clear();
        this.moneyAtStartOfRound = this.getMoney();
        this.totalRoundsPlayed++;
        this.hasFolded = false;
        this.lostInShowdown = false;
        this.isAllIn = false;
        this.roundQ = Math.random();
        this.roundOdds.clear();
        
        this.gui_updateLabelCards(0);
        this.gui_updateLabelMoney();
        this.gui_setTextAreaColor(1, 0);
        this.gui_setTextAreaColor(2, 0);
        this.gui_setTextArea(1, "");
        this.gui_showOdds(false);
    }
    
    /**
     * Set player for a new betting round.
     */
    public void setForBettingRoundStart()
    {
        this.moneyOnTable = 0;
        this.hasBeenAskedInBettingRound = false;
        
        this.gui_updateLabelMoney();
    }
    
    /**
     * A player looses a game when he has no money left.
     * @return 
     */
    public boolean hasLostGame()
    {
        return (this.hasLeftGame
                || (this.moneyInPocket == 0 && this.moneyOnTable == 0));
    }
    
    /**
     * A player looses a round when he folds.
     * @return 
     */
    public boolean hasLostRound()
    {
        return (this.hasFolded || this.lostInShowdown);
    }
    
    /**
     * A player is finished when he folded, is all in or called.
     * @param amountToCall
     * @return 
     */
    public boolean isFinishedForBettingRound(int amountToCall)
    {
        if( this.hasLostRound() )
            return true;
        else if(this.isAllIn)
            return true;
        else if( !this.hasBeenAskedInBettingRound )
            return false;
        else
            //has called if the moneyOnTable equals amountToCall
            return (this.moneyOnTable == amountToCall);
    }
    
    /**
     * Is all in.
     * @return 
     */
    public boolean IsAllIn()
    {
        return this.isAllIn;
    }
    
    /**
     * Add a card to players hand.
     * @param c 
     */
    public void addCard(Card c)
    {
        this.hand.addCard(c);
    }
    
    /**
     * The player can decide if he wants to continue to next round or exit.
     * @return
     */
    public boolean goForNextRound()
    {
        boolean continueGame = true;
        
        if(this.strategy == Player.IS_USER && this.withGUI)
        {
            Boolean[] returnVal = new Boolean[1];
            
            //pop up window
            new NextRoundDialog(this.parentFrame, returnVal);
            
            continueGame = returnVal[0];
        }
        
        if( !continueGame )
            this.hasLeftGame = true;
        
        return continueGame;
    }
    
    /**
     * Make a decision about the action to take.
     * @param preFlopOdds
     * @param type
     * @param cardsOnTable
     * @param amountToCall
     * @param betSize
     * @param qtyRaises
     * @return 
     */
    public ActionType chooseAction(Map<String, Double> preFlopOdds,
            BettingRoundType type, Hand cardsOnTable, int amountToCall,
            int betSize, int qtyRaises)
    {
        this.calculateOdds(preFlopOdds, type, cardsOnTable);
        
        ActionType at;
        int thisCase;
        
        if(amountToCall == this.moneyOnTable)
            thisCase = 1;           //no current amount to call
        else    //has to call
        {
            if((this.moneyInPocket + this.moneyOnTable) < amountToCall)
                thisCase = 2;       //amount to call is bigger than money
            else
            {
                if((this.moneyInPocket + this.moneyOnTable - amountToCall) < betSize)
                    thisCase = 3;   //has enough money to call, but not for a raise
                else
                    thisCase = 4;   //has enough money to call or raise
            }
        }
        
        if(this.strategy == Player.IS_USER)
        {
            if(this.withGUI)
            {
                ActionType[] returnVar = new ActionType[1];
                
                //popup window asking for user choice
                new UsersChoiceDialog(this.parentFrame, thisCase, betSize,
                        returnVar);
                
                at = returnVar[0];
            }
            else
            {
                at = ActionType.FOLD;   //does not make sense to play without GUI
            }
        }
        else
        {
            if(this.roundQ < this.q)
            {
                //ask PC for action
                at = Strategy.getRecomendedAction(this.roundOdds.get(type),
                        type, cardsOnTable, this.hand, this.strategy, thisCase,
                        qtyRaises);
            }
            else
            {
                switch(thisCase)
                {
                    case 1:     //no current amount to call
                        at = ActionType.RAISE;
                        break;
                    case 2:     //amount to call is bigger than money
                        at = ActionType.CALL;   //go all in
                        break;
                    case 3:     //has enough money to call, but not for a raise
                        at = ActionType.CALL;
                        break;
                    default:    //has enough money to call or raise
                        at = ActionType.RAISE;
                }
                
                if(at == ActionType.RAISE)
                {
                    double prob = 1 / Math.pow(2, qtyRaises);
                    double k = Math.random();
                    if (k > prob)
                        at = ActionType.CALL;
                } 
            }
        }
        
        //validate at, so that there will never be an invalid action for each case
        switch(thisCase)
        {
            case 1:     //no current amount to call
                if(at == ActionType.CALL)
                    at = ActionType.CHECK;
                else if(qtyRaises >= Game.MAX_QTY_RAISES && at == ActionType.RAISE)
                    at = ActionType.CHECK;
                break;
            case 2:     //amount to call is bigger than money
                if(at == ActionType.CHECK || at == ActionType.RAISE)
                    at = ActionType.FOLD;
                break;
            case 3:     //has enough money to call, but not for a raise
                if(at == ActionType.CHECK || at == ActionType.RAISE)
                    at = ActionType.CALL;
                break;
            default:    //has enough money to call or raise
                if(at == ActionType.CHECK)
                    at = ActionType.CALL;
                else if(qtyRaises >= Game.MAX_QTY_RAISES && at == ActionType.RAISE)
                    at = ActionType.CALL;
        }
        return at;
    }
    
    /**
     * Calculate the odds.
     * @param preFlopOdds 
     * @param type
     * @param cardsOnTable
     */
    private void calculateOdds(Map<String, Double> preFlopOdds,
            BettingRoundType type, Hand cardsOnTable)
    {
        Odds odds;
        
        switch(type)
        {
            case PRE_FLOP:
                odds = OddsCalculator.preFlop(this.hand, preFlopOdds);
                break;
            case FLOP:
                odds = OddsCalculator.flop(this.hand, cardsOnTable);
                break;
            case TURN:
                odds = OddsCalculator.turn(this.hand, cardsOnTable);
                break;
            default:
                odds = OddsCalculator.river(this.hand, cardsOnTable);
        }
        
        this.roundOdds.put(type, odds);
        this.gui_showOdds(false);
    }
    
    /**
     * Put blind on the table.
     * @param blindAmount
     */
    public void putBlind(int blindAmount)
    {
        this.transferMoneyToTable(blindAmount);
        
        String s = "Blind " + blindAmount;
        if(blindAmount == Game.SMALL_BLIND)
            s += " (is DEALER)";
        
        this.gui_addToTextArea(1, s, BettingRoundType.PRE_FLOP);
    }
    
    /**
     * @param type
     * Check.
     */
    public void check(BettingRoundType type)
    {
        this.hasBeenAskedInBettingRound = true;
        
        this.gui_addToTextArea(1, "Check", type);
    }
    
    /**
     * Raise.
     * @param type
     * @param amountToCall
     * @param additionalBet
     */
    public void raise(BettingRoundType type, int amountToCall, int additionalBet)
    {
        this.hasBeenAskedInBettingRound = true;
        this.transferMoneyToTable(amountToCall - this.moneyOnTable
                + additionalBet);
        
        this.gui_addToTextArea(1, "Raise " + additionalBet, type);
    }
    
    /**
     * Call.
     * @param type
     * @param amountToCall
     */
    public void call(BettingRoundType type, int amountToCall)
    {
        this.hasBeenAskedInBettingRound = true;
        int aux = amountToCall - this.moneyOnTable;
        this.transferMoneyToTable(aux);
        
        this.gui_addToTextArea(1, "Call " + aux, type);
    }
    
    /**
     * Fold.
     * @param type
     */
    public void fold(BettingRoundType type)
    {
        this.hasBeenAskedInBettingRound = true;
        this.hasFolded = true;
        this.roundsLost++;
        this.totalRoundsLost++;
        this.gameMoneyLost += (this.moneyAtStartOfRound - this.getMoney());
        this.totalMoneyLost += (this.moneyAtStartOfRound - this.getMoney());
        
        this.gui_setTextAreaColor(1, 2);
        this.gui_addToTextArea(1, "Fold", type);
    }
    
    /**
     * Lost in the showdown.
     */
    public void lostShowdown()
    {
        this.lostInShowdown = true;
        this.roundsLost++;
        this.totalRoundsLost++;
        this.gameMoneyLost += (this.moneyAtStartOfRound - this.getMoney());
        this.totalMoneyLost += (this.moneyAtStartOfRound - this.getMoney());
        
        this.gui_updateLabelCards(1);
        this.gui_setTextAreaColor(1, 2);
        this.gui_addToTextArea("LOSER");
    }
    
    /**
     * Remove the money on the table after a betting round (it will be
     * placed in the pot).
     * @return 
     */
    public int removeMoneyOnTable()
    {
        int aux = this.moneyOnTable;
        this.moneyOnTable = 0;
        return aux;
    }
    
    /**
     * Tranfer more money from pocket to table.
     * @param money
     */
    private void transferMoneyToTable(int money)
    {
        if(this.moneyInPocket > money)
        {
            //raise or call
            this.moneyOnTable += money;
            this.moneyInPocket -= money;
        }
        else
        {
            //goes all in
            this.isAllIn = true;
            this.moneyOnTable += this.moneyInPocket;
            this.moneyInPocket = 0;
        }
    }
    
    /**
     * Won a round.
     * @param money
     */
    public void wonRound(int money)
    {
        this.moneyInPocket += money;
        this.roundsWon++;
        this.totalRoundsWon++;
        this.gameMoneyWon += (this.getMoney() - this.moneyAtStartOfRound);
        this.totalMoneyWon += (this.getMoney() - this.moneyAtStartOfRound);
        
        this.gui_updateLabelCards(1);
        this.gui_setTextAreaColor(1, 3);
        this.gui_addToTextArea(String.format("WINNER +%,d => total: %,d",
                money, this.moneyInPocket));
    }
    
    /**
     * Get money of player.
     * @return 
     */
    public int getMoney()
    {
        return this.moneyInPocket + this.moneyOnTable;
    }
    
    /**
     * Get money on table of player.
     * @return 
     */
    public int getMoneyOnTable()
    {
        return this.moneyOnTable;
    }
    
    /**
     * Get hand.
     * @return 
     */
    public Hand getHand()
    {
        return this.hand;
    }
    
    public int getTotalRoundsPlayed() {
        return totalRoundsPlayed;
    }
    
    public int getTotalRoundsWon() {
        return totalRoundsWon;
    }
    
    public int getTotalRoundsLost() {
        return totalRoundsLost;
    }
    
    public int getTotalGamesPlayed() {
        return totalGamesPlayed;
    }
    
    public int getTotalGamesWon() {
        return totalGamesWon;
    }
    
    public int getTotalGamesLost() {
        return totalGamesLost;
    }
    
    public void increaseGamesWon() {
        this.totalGamesWon++;
    }
    
    public void increaseGamesLost() {
        this.totalGamesLost++;
    }
    
    public int getTotalMoneyWon() {
        return totalMoneyWon;
    }
    
    public int getTotalMoneyLost() {
        return totalMoneyLost;
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }
    
    public String getGameStatistics()
    {
        return String.format(
                "%s [money=%,7d] [WON r=%3d, m=%,7d] [LOST r=%3d, m=%,7d]",
                this.name, this.getMoney(),
                this.roundsWon, this.gameMoneyWon,
                this.roundsLost, this.gameMoneyLost);
    }
    
    public String getOddsToWin()
    {
        String s = "";
        for(Map.Entry<BettingRoundType, Odds> e: this.roundOdds.entrySet())
        {
            s += String.format(" - %4.2f", e.getValue().getWins());
        }
        if( !s.isEmpty() )
            s = s.substring(3);
        
        return "oddsToWin(" + s + ")";
    }
    
    /**
     * Update GUI at end of game.
     */
    public void updateGUI()
    {
        if(this.withGUI)
        {
            this.gui_updateLabelMoney();
            this.gui_setTextArea(2, "");
            
            if(this.hasLostGame())
            {
                this.gui_setTextAreaColor(1, 2);
                if(this.hasLeftGame)
                    this.gui_setTextArea(1, "This player left the game.");
                this.gui_addToTextArea("\n\nLOSER");
            }
            else
            {
                this.gui_setTextAreaColor(1, 3);
                this.gui_setTextArea(1, "\n\nWINNER");
            }
        }
    }
    
    /**
     * Update GUI.
     * @param t
     */
    public void updateGUI(BettingRoundType t)
    {
        if(this.withGUI)
        {
            if(t == null)
                this.gui_updateLabelCards(1);   //show cards at end of round
            else if(this.strategy == Player.IS_USER && t == BettingRoundType.PRE_FLOP)
                this.gui_updateLabelCards(1);
            
            this.gui_updateLabelMoney();
        }
    }
    
    /**
     * Updates labels of money and bet of this player in the GUI.
     */
    private void gui_updateLabelName()
    {
        if(this.withGUI)
        {
            String s;
            if(this.strategy == Player.IS_USER)
                s = String.format("%s (USER)", this.name);
            else
                s = String.format("%s (PC)", this.name);
            
            this.labelName.setText(s);
        }
    }
    
    /**
     * Updates labels of the cards of this player in the GUI.
     *  i==0: back of cards
     *  i==1: show cards
     *  else: game over
     * @param i 
     */
    private void gui_updateLabelCards(int i)
    {
        if(this.withGUI)
        {
            ImageIcon i1, i2;
            if(i == 0)
            {
                i1 = new ImageIcon(getClass().getResource("/resources/"
                        + PokerGUI.BACK_OF_CARDS_IMAGE_FILE));
                i2 = new ImageIcon(getClass().getResource("/resources/"
                        + PokerGUI.BACK_OF_CARDS_IMAGE_FILE));
            }
            else if(i == 1)
            {
                i1 = new ImageIcon(getClass().getResource("/resources/"
                        + this.getHand().getCard(0).getNameOfImageFile()));
                i2 = new ImageIcon(getClass().getResource("/resources/"
                        + this.getHand().getCard(1).getNameOfImageFile()));
            }
            else    //game over for this player
            {
                i1 = new ImageIcon(getClass().getResource("/resources/"
                        + PokerGUI.PLAYER_OUT_IMAGE_FILE));
                i2 = new ImageIcon(getClass().getResource("/resources/"
                        + PokerGUI.PLAYER_OUT_IMAGE_FILE));
            }
            labelCard1.setIcon(i1);
            labelCard2.setIcon(i2);
        }
    }
    
    /**
     * Updates labels of money and bet of this player in the GUI.
     */
    private void gui_updateLabelMoney()
    {
        if(this.withGUI)
        {
            this.labelMoney.setText(String.format("%,d (%.1f %%)",
                    this.moneyInPocket,
                    ((double)this.moneyInPocket * 100) / Game.TOTAL_MONEY));
            this.labelBet.setText(String.format("%,d (%.1f %%)",
                    this.moneyOnTable,
                    ((double)this.moneyOnTable * 100) / Game.TOTAL_MONEY));
        }
    }
    
    /**
     * Updates the textAreas of this player in the GUI.
     * @param i 
     * @param s 
     */
    private void gui_setTextArea(int i, String s)
    {
        if(this.withGUI)
        {
            if (i == 1)
                this.textArea1.setText(s);
            else
                this.textArea2.setText(s);
        }
    }
    
    /**
     * Updates the textAreas of this player in the GUI.
     * @param i 
     * @param s 
     * @param type
     */
    private void gui_addToTextArea(int i, String s, BettingRoundType type)
    {
        if(this.withGUI)
        {
            if (i == 1)
                this.textArea1.append(type + ": " + s + "\n");
            else
                this.textArea2.append(type + ": " + s + "\n");
        }
    }
    
    /**
     * Updates the textAreas of this player in the GUI.
     * @param s 
     */
    public void gui_addToTextArea(String s)
    {
        if(this.withGUI)
        {
            this.textArea1.append(s + "\n");
        }
    }
    
    /**
     * Changes the backround color of the textArea of this player in the GUI.
     *  opt==1: dark grey
     *  opt==2: dark red
     *  opt==3: dark blue
     *  else: default green
     * @param i 
     * @param opt 
     */
    private void gui_setTextAreaColor(int i, int opt)
    {
        if(this.withGUI)
        {
            java.awt.Color c;
            if(opt == 1)
                c = new java.awt.Color(102, 102, 102);  //dark grey
            else if(opt == 2)
                c = new java.awt.Color(153, 0, 0);      //dark red
            else if(opt == 3)
                c = new java.awt.Color(0, 0, 102);      //dark blue
            else
                c = new java.awt.Color(0, 204, 0);      //default green

            if (i == 1)
                this.textArea1.setBackground(c);
            else
                this.textArea2.setBackground(c);
        }
    }
    
    /**
     * Display the odds the player had so far in this round.
     * @param endOfRound
     */
    public void gui_showOdds(boolean endOfRound)
    {
        if(endOfRound || this.strategy == Player.IS_USER)
        {
            String s = "Odds to win:\n";
            for(Map.Entry<BettingRoundType, Odds> e: this.roundOdds.entrySet())
            {
                s += String.format("  %-10s %6.2f / 100\n", e.getKey(), e.getValue().getWins());
            }
            this.gui_setTextArea(2, s);
        }
        else
        {
            this.gui_setTextArea(2, "You will see these odds at the end of the round.");
        }
    }
}
