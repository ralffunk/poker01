package enums;

/**
 * All betting round types.
 * @author Ralf
 */
public enum BettingRoundType
{
    PRE_FLOP,
    FLOP,
    TURN,
    RIVER;
}
