package concepts;

import enums.ActionType;
import enums.BettingRoundType;

/**
 * The AI strategy for a poker player.
 */
public class StrategyThree
{
    /**
     * AI.
     * @param type
     * @param cardsOnTable
     * @param cardsOfPlayer
     * @param thisCase
     * @return 
     */
    public static ActionType getRecomendedAction(BettingRoundType type,
            Hand cardsOnTable, Hand cardsOfPlayer, int thisCase)
    {
        ActionType at;
        switch(thisCase)
        {
            case 1:     //no current amount to call
                at = ActionType.CHECK;
                break;
            case 2:     //amount to call is bigger than money
                at = ActionType.FOLD;
                break;
            case 3:     //has enough money to call, but not for a raise
                at = ActionType.FOLD;
                break;
            default:    //has enough money to call or raise
                at = ActionType.FOLD;
                break;
        }
        
        return at;
    }
}
