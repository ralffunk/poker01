package concepts;

import enums.BettingRoundType;
import enums.ActionType;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import utils.Logger;

/**
 * A round.
 * @author Ralf
 */
public class BettingRound
{
    /**
     * Attributes.
     */
    private final Logger logger;
    private final List<Player> players;
    private final Map<String, Double> preFlopOdds;
    private final BettingRoundType type;
    private final Deck deck;
    private final Hand cardsOnTable;
    private final int pot;
    private int amountToCall;
    private final boolean withGUI;
    private final JLabel labelAmountToCall;
    private final List<JLabel> cardLabels;
    
    /**
     * Constructor.
     * @param logger
     * @param players
     * @param preFlopOdds
     * @param type
     * @param deck
     * @param cardsOnTable
     * @param pot
     * @param cardLabels
     * @param labelAmountToCall 
     */
    public BettingRound(Logger logger, List<Player> players,
            Map<String, Double> preFlopOdds, BettingRoundType type, Deck deck,
            Hand cardsOnTable, int pot, List<JLabel> cardLabels,
            JLabel labelAmountToCall)
    {
        this.logger = logger;
        this.players = players;
        this.preFlopOdds = preFlopOdds;
        this.type = type;
        this.deck = deck;
        this.cardsOnTable = cardsOnTable;
        this.pot = pot;
        
        this.cardLabels = cardLabels;
        this.labelAmountToCall = labelAmountToCall;
        
        this.withGUI = (this.cardLabels != null);
    }
    
    /**
     * Play the betting round.
     * @return the money to increase the pot
     */
    public int play()
    {
        this.logger.setBettingRound(this.type);
        
        boolean bettingRoundIsOver = false,
                askAgain;
        int playersStillPlaying, qtyRaises = 0;
        ActionType at;
        
        //initialize players for betting round
        for(Player p: this.players)
        {
            p.setForBettingRoundStart();
        }
        
        //deal cards
        this.startBettingRound();
        this.gui_updateCards();
        this.gui_updateLabels();
        
        while( !bettingRoundIsOver )
        {
            playersStillPlaying = this.players.size();
            askAgain = false;
            for(Player p: this.players)
            {
                if(playersStillPlaying > 1)
                {
                    if( !p.isFinishedForBettingRound(this.amountToCall) )
                    {
                        askAgain = true;
                        
                        p.updateGUI(this.type);
                        at = p.chooseAction(this.preFlopOdds, this.type,
                                this.cardsOnTable, this.amountToCall, this.getBetSize(),
                                qtyRaises);
                        
                        switch(at)
                        {
                            case CHECK:
                                p.check(this.type);
                                break;
                            case RAISE:
                                p.raise(this.type, this.amountToCall,
                                        this.getBetSize());
                                this.amountToCall += this.getBetSize();
                                qtyRaises++;
                                break;
                            case CALL:
                                p.call(this.type, this.amountToCall);
                                break;
                            default:
                                p.fold(this.type);
                                playersStillPlaying--;
                                break;
                        }
                        this.logger.print(String.format(
                                "%s %-7s moneyOnTable=%5d amountToCall=%d",
                                p, at, p.getMoneyOnTable(), this.amountToCall));
                        this.gui_updateLabels();
                    }
                }
            }
            
            if( !askAgain || playersStillPlaying < 2 )
                bettingRoundIsOver = true;
        }
        
        this.endBettingRound();
        
        //collect moneyOnTable from each player and put it into pot
        int bettingRoundPot = 0;
        for(Player p: this.players)
        {
            bettingRoundPot += p.removeMoneyOnTable();
            p.updateGUI(this.type);
        }
        
        this.logger.unsetBettingRound(bettingRoundPot);
        return bettingRoundPot;
    }
    
    /**
     * Do specific tasks for each betting round type.
     */
    private void startBettingRound()
    {
        switch(this.type)
        {
            case PRE_FLOP:
                //deal cards for each player
                for(int i=0; i<2; i++)
                {
                    for(Player p: this.players)
                    {
                        p.addCard( this.deck.dealCard() );
                    }
                    this.deck.dealCard();    //discard one card
                }
                this.amountToCall = Game.BIG_BLIND;
                
                // ONLY for 2 players
                // get blinds from players
                this.players.get(0).putBlind(Game.BIG_BLIND);
                this.players.get(1).putBlind(Game.SMALL_BLIND); //dealer
                
                this.players.get(0).updateGUI(type);
                this.players.get(1).updateGUI(type);
                
                this.logger.print(String.format(
                        "%s %-7s moneyOnTable=%5d amountToCall=%d",
                        this.players.get(1), "S_BLIND",
                        this.players.get(1).getMoneyOnTable(),
                        this.amountToCall));
                this.logger.print(String.format(
                        "%s %-7s moneyOnTable=%5d amountToCall=%d",
                        this.players.get(0), "B_BLIND",
                        this.players.get(0).getMoneyOnTable(),
                        this.amountToCall));
                
                //move dealer to front, because he has to decide first
                Collections.rotate(this.players, 1);
                
                break;
            case FLOP:
                //deal cards for table
                for(int i=0; i<3; i++)
                {
                    this.cardsOnTable.addCard( this.deck.dealCard() );
                    this.deck.dealCard();    //discard one card
                }
                this.amountToCall = 0;
                break;
            case TURN:
                this.cardsOnTable.addCard( this.deck.dealCard() );
                this.deck.dealCard();       //discard one card
                this.amountToCall = 0;
                break;
            default:
                this.cardsOnTable.addCard( this.deck.dealCard() );
                this.amountToCall = 0;
                break;
        }
    }
    
    /**
     * Do specific tasks for each betting round type.
     */
    private void endBettingRound()
    {
        switch(this.type)
        {
            case PRE_FLOP:
                //move dealer to end again
                Collections.rotate(this.players, -1);
                break;
            default:
                break;
        }
    }
    
    /**
     * Returns the size of a bet.
     * In Poker Texas Holdem with Limit the size of the bet is fixed and
     * also debends on the betting round.
     * @return 
     */
    private int getBetSize()
    {
        int bet;
        switch(this.type)
        {
            case PRE_FLOP:
                bet = Game.SMALL_BET;
                break;
            case FLOP:
                bet = Game.SMALL_BET;
                break;
            case TURN:
                bet = Game.BIG_BET;
                break;
            default:
                bet = Game.BIG_BET;
                break;
        }
        
        return bet;
    }
    
    /**
     * Updates GUI.
     */
    private void gui_updateCards()
    {
        if(this.withGUI)
        {
            switch(this.type)
            {
                case FLOP:
                    for(int i=0; i<3; i++)
                    {
                        this.cardLabels.get(i).setIcon( new ImageIcon(getClass()
                                .getResource("/resources/" + this.cardsOnTable
                                        .getCard(i).getNameOfImageFile())) );
                    }
                    break;
                case TURN:
                    this.cardLabels.get(3).setIcon( new ImageIcon(getClass()
                            .getResource("/resources/" + this.cardsOnTable
                                    .getCard(3).getNameOfImageFile())) );
                    break;
                case RIVER:
                    this.cardLabels.get(4).setIcon( new ImageIcon(getClass()
                            .getResource("/resources/" + this.cardsOnTable
                                    .getCard(4).getNameOfImageFile())) );
                    break;
                default:
                    //on PRE_FLOP do nothing
                    break;
            }
        }
    }
    
    /**
     * Updates GUI.
     */
    private void gui_updateLabels()
    {
        if(this.withGUI)
        {
            this.labelAmountToCall.setText(String.format("%,d (%.1f %%)",
                    this.amountToCall,
                    ((double)this.amountToCall * 100) / Game.TOTAL_MONEY));
        }
    }
}
