package concepts;

import enums.Combination;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Evaluate hands.
 * @author Ralf
 */
public class HandEvaluator
{
    /**
     * Evaluates a hand.
     * @param table
     * @param player
     * @param outComb
     * @param outCombDsc
     * @param outHand
     */
    public static void evaluate(Hand table, Hand player,
            Combination[] outComb, String[] outCombDsc, Hand[] outHand)
    {
        //some initial checking
        if(table.getCardCount() != 5)
            throw new RuntimeException("got table with "
                    + table.getCardCount() + " cards");
        if(player.getCardCount() != 2)
            throw new RuntimeException("got player with "
                    + player.getCardCount() + " cards");
        if(outComb.length != 1)
            throw new RuntimeException("got outComb with length "
                    + outComb.length);
        if(outCombDsc.length != 1)
            throw new RuntimeException("got outCombDsc with length "
                    + outCombDsc.length);
        if(outHand.length != 1)
            throw new RuntimeException("got outHand with length "
                    + outHand.length);
        
        //make a single hand
        int i;
        Hand hand = new Hand(7);
        for(i=0; i<5; i++)
            hand.addCard(table.getCard(i));
        for(i=0; i<2; i++)
            hand.addCard(player.getCard(i));
        hand.sortByValue();
        
        //aux variables
        Hand auxStraightFlush = new Hand(5);
        Hand auxPoker = new Hand(4);
        Hand auxFlush = new Hand(7);
        Hand auxStraight = new Hand(5);
        List<Hand> auxTrios = new LinkedList<>();
        List<Hand> auxPairs = new LinkedList<>();
        
        //count ranks and suits
        Map<Integer, Integer> ranks = new HashMap<>();
        for(i=2; i<15; i++)
            ranks.put(i, 0);
        Map<Integer, Integer> suits = new HashMap<>();
        for(i=1; i<5; i++)
            suits.put(i, 0);
        
        Card c2;
        for(i=0; i<7; i++)
        {
            c2 = hand.getCard(i);
            ranks.put(c2.getRank(), ranks.get(c2.getRank()) + 1);
            suits.put(c2.getSuit(), suits.get(c2.getSuit()) + 1);
        }
        
        //test for a flush
        for(int s: suits.keySet())
        {
            if(suits.get(s) >= 5)
            {
                for(Card c: hand.getCards())
                {
                    if(c.getSuit() == s)
                        auxFlush.addCard(c);
                }
                auxFlush.sortByValue();
            }
        }
        
        //test if flush is also a straightFlush
        if(auxFlush.getCardCount() > 0)
        {
            for(i=0; i <= auxFlush.getCardCount()-5; i++)
            {
                int auxRank = auxFlush.getCard(i).getRank();
                if(auxRank+1 == auxFlush.getCard(i+1).getRank()
                        && auxRank+2 == auxFlush.getCard(i+2).getRank()
                        && auxRank+3 == auxFlush.getCard(i+3).getRank()
                        && auxRank+4 == auxFlush.getCard(i+4).getRank())
                {
                    auxStraightFlush.clear();
                    for(int j=i; j < i+5; j++)
                    {
                        auxStraightFlush.addCard(auxFlush.getCard(j));
                    }
                    auxStraightFlush.sortByValue();
                }
            }
        }
        
        //test for straight, poker, trio and pairs
        Hand auxHand;
        for(int r: ranks.keySet())
        {
            if(ranks.get(r) == 4)
            {
                auxPoker.clear();
                for(Card c: hand.getCards())
                {
                    if(c.getRank() == r)
                        auxPoker.addCard(c);
                }
                auxPoker.sortByValue();
            }
            else if(ranks.get(r) == 3)
            {
                auxHand = new Hand(3);
                for(Card c: hand.getCards())
                {
                    if(c.getRank() == r)
                        auxHand.addCard(c);
                }
                auxHand.sortByValue();
                auxTrios.add(0, auxHand);
            }
            else if(ranks.get(r) == 2)
            {
                auxHand = new Hand(2);
                for(Card c: hand.getCards())
                {
                    if(c.getRank() == r)
                        auxHand.addCard(c);
                }
                auxHand.sortByValue();
                auxPairs.add(0, auxHand);
            }
            
            if(r <= 10)
            {
                //check whether the next 4 cards also are in the hand
                if(ranks.get(r) > 0 && ranks.get(r+1) > 0 && ranks.get(r+2) > 0
                        && ranks.get(r+3) > 0 && ranks.get(r+4) > 0)
                {
                    //take only one card if there are a few of the same rank
                    Card[] auxCards = new Card[5];
                    for(Card c: hand.getCards())
                    {
                        if(r <= c.getRank() && c.getRank() <= r+4)
                        {
                            if(auxCards[c.getRank() - r] == null)
                                auxCards[c.getRank() - r] = c;
                            else if(auxCards[c.getRank() - r].getSuit() < c.getSuit())
                                auxCards[c.getRank() - r] = c;
                        }
                    }
                    auxStraight.clear();
                    for(i=0; i<5; i++)
                    {
                        auxStraight.addCard(auxCards[i]);
                    }
                    auxStraight.sortByValue();
                }
            }
        }
        
        //determine the main combination of the hand and its cards
        outHand[0] = new Hand(5);
        if(auxStraightFlush.getCardCount() > 0)
        {
            outComb[0] = Combination.STRAIGHT_FLUSH;
            outHand[0].clear();
            for(Card c: auxStraightFlush.getCards())
            {
                outHand[0].addCard(c);
            }
            outHand[0].sortByValue();
            outCombDsc[0] = outComb[0].toString();
        }
        else if(auxPoker.getCardCount() > 0)
        {
            outComb[0] = Combination.POKER;
            outHand[0].clear();
            for(Card c: auxPoker.getCards())
            {
                outHand[0].addCard(c);
            }
            Set<Card> wh = new HashSet<>(outHand[0].getCards());
            Set<Card> h = new HashSet<>(hand.getCards());
            h.removeAll(wh);
            auxHand = new Hand(3);
            for(Card c: h)
            {
                auxHand.addCard(c);
            }
            auxHand.sortByValue();
            outHand[0].addCard(auxHand.getCard(2), 0);
            outCombDsc[0] = outComb[0].toString();
        }
        else if( !auxTrios.isEmpty() && !auxPairs.isEmpty() )
        {
            outComb[0] = Combination.FULL_HOUSE;
            outHand[0].clear();
            outHand[0].addCard(auxTrios.get(0).getCard(0));
            outHand[0].addCard(auxTrios.get(0).getCard(1));
            outHand[0].addCard(auxTrios.get(0).getCard(2));
            outHand[0].addCard(auxPairs.get(0).getCard(0), 0);
            outHand[0].addCard(auxPairs.get(0).getCard(1), 1);
            outCombDsc[0] = outComb[0].toString();
        }
        else if(auxFlush.getCardCount() > 0)
        {
            outComb[0] = Combination.FLUSH;
            outHand[0].clear();
            for(i = auxFlush.getCardCount()-5; i < auxFlush.getCardCount(); i++)
            {
                outHand[0].addCard( auxFlush.getCard(i) );
            }
            outCombDsc[0] = outComb[0].toString();
        }
        else if(auxStraight.getCardCount() > 0)
        {
            outComb[0] = Combination.STRAIGHT;
            outHand[0].clear();
            for(i = auxStraight.getCardCount()-5; i < auxStraight.getCardCount(); i++)
            {
                outHand[0].addCard( auxStraight.getCard(i) );
            }
            outCombDsc[0] = outComb[0].toString();
        }
        else if( !auxTrios.isEmpty() )
        {
            outComb[0] = Combination.TRIO;
            outHand[0].clear();
            for(Card c: auxTrios.get(0).getCards())
            {
                outHand[0].addCard(c);
            }
            Set<Card> wh = new HashSet<>(outHand[0].getCards());
            Set<Card> h = new HashSet<>(hand.getCards());
            h.removeAll(wh);
            auxHand = new Hand(4);
            for(Card c: h)
            {
                auxHand.addCard(c);
            }
            auxHand.sortByValue();
            outHand[0].addCard(auxHand.getCard(3), 0);
            outHand[0].addCard(auxHand.getCard(2), 0);
            outCombDsc[0] = outComb[0].toString();
        }
        else if(auxPairs.size() > 1)
        {
            outComb[0] = Combination.TWO_PAIRS;
            outHand[0].clear();
            outHand[0].addCard(auxPairs.get(0).getCard(0), 0);
            outHand[0].addCard(auxPairs.get(0).getCard(1), 1);
            outHand[0].addCard(auxPairs.get(1).getCard(0), 0);
            outHand[0].addCard(auxPairs.get(1).getCard(1), 1);
            Set<Card> wh = new HashSet<>(outHand[0].getCards());
            Set<Card> h = new HashSet<>(hand.getCards());
            h.removeAll(wh);
            auxHand = new Hand(3);
            for(Card c: h)
            {
                auxHand.addCard(c);
            }
            auxHand.sortByValue();
            outHand[0].addCard(auxHand.getCard(2), 0);
            outCombDsc[0] = outComb[0].toString();
        }
        else if( !auxPairs.isEmpty() )
        {
            outComb[0] = Combination.ONE_PAIR;
            outHand[0].clear();
            outHand[0].addCard(auxPairs.get(0).getCard(0), 0);
            outHand[0].addCard(auxPairs.get(0).getCard(1), 1);
            Set<Card> wh = new HashSet<>(outHand[0].getCards());
            Set<Card> h = new HashSet<>(hand.getCards());
            h.removeAll(wh);
            auxHand = new Hand(5);
            for(Card c: h)
            {
                auxHand.addCard(c);
            }
            auxHand.sortByValue();
            outHand[0].addCard(auxHand.getCard(4), 0);
            outHand[0].addCard(auxHand.getCard(3), 0);
            outHand[0].addCard(auxHand.getCard(2), 0);
            outCombDsc[0] = outComb[0].toString();
        }
        else
        {
            outComb[0] = Combination.H_CARD;
            outHand[0].clear();
            for(i=2; i<7; i++)
            {
                outHand[0].addCard( hand.getCard(i) );
            }
            outCombDsc[0] = outComb[0].toString();
        }
    }
    
    /**
     * Get winners.
     * @param players
     * @param combs
     * @param hands
     * @param outPlayers
     * @param outCombs
     * @param outHands
     * @return 
     */
    public static boolean[] get_winners(List<Player> players, List<Combination> combs,
            List<Hand> hands, List<Player> outPlayers, List<Combination> outCombs,
            List<Hand> outHands)
    {
        //some initial checking
        if(players.size() != combs.size() || combs.size() != hands.size())
            throw new RuntimeException("different length");
        outPlayers.clear();
        outCombs.clear();
        outHands.clear();
        
        int qtyWinners, i, n = players.size();
        boolean[] posWinners = new boolean[n];
        
        //find max combination
        Combination maxComb = Collections.max(combs);
        
        //find players with max comb
        qtyWinners = 0;
        for(i=0; i<n; i++)
        {
            if(combs.get(i) == maxComb)
            {
                posWinners[i] = true;
                qtyWinners++;
            }
        }
        
        if(qtyWinners == 1)
        {
            for(i=0; i<n; i++)
            {
                if(posWinners[i])
                {
                    outPlayers.add(players.get(i));
                    outCombs.add(combs.get(i));
                    outHands.add(hands.get(i));
                }
            }
        }
        else    //many players with max comb
        {
            int[] auxWinners = new int[n];
            int auxMax;
            
            //compare all 5 cards in the hands. this is not the optimal
            //solution, but its less code
            for(int k=4; k >= 0; k--)
            {
                //determine which players have the top k card
                for(i=0; i<n; i++)
                {
                    if(posWinners[i])
                        auxWinners[i] = hands.get(i).getCard(k).getRank();
                }
                auxMax = 0;
                for(i=0; i<n; i++)
                {
                    if(posWinners[i])
                        if(auxMax < auxWinners[i])
                            auxMax = auxWinners[i];
                }
                for(i=0; i<n; i++)
                {
                    if(posWinners[i])
                        posWinners[i] = (hands.get(i).getCard(k).getRank() == auxMax);
                }
            }
            //get all players that are winners
            for(i=0; i<n; i++)
            {
                if(posWinners[i])
                {
                    outPlayers.add(players.get(i));
                    outCombs.add(combs.get(i));
                    outHands.add(hands.get(i));
                }
            }
        }
        
        return posWinners;
    }
    
    /**
     * Determine the winner between two players.
     * @param hTable
     * @param hP1
     * @param hP2
     * @return 
     */
    public static int getWinner(Hand hTable, Hand hP1, Hand hP2)
    {
        List<Player> players = new LinkedList<>(),
                outPlayers = new LinkedList<>();
        List<Combination> combs = new LinkedList<>(),
                outCombs = new LinkedList<>();
        List<Hand> hands = new LinkedList<>(),
                outHands = new LinkedList<>();
        Combination[] returnComb = new Combination[1];
        String[] returnCombDsc = new String[1];
        Hand[] returnHand = new Hand[1];
        boolean[] posWinners;
        
        returnComb[0] = null;
        returnCombDsc[0] = null;
        returnHand[0] = null;
        HandEvaluator.evaluate(hTable, hP1, returnComb, returnCombDsc, returnHand);
        players.add( new Player("P1", Player.STRATEGY_1) );
        combs.add(returnComb[0]);
        hands.add(returnHand[0]);
        
        returnComb[0] = null;
        returnCombDsc[0] = null;
        returnHand[0] = null;
        HandEvaluator.evaluate(hTable, hP2, returnComb, returnCombDsc, returnHand);
        players.add( new Player("P2", Player.STRATEGY_1) );
        combs.add(returnComb[0]);
        hands.add(returnHand[0]);
        
        posWinners = HandEvaluator.get_winners(players, combs, hands,
                outPlayers, outCombs, outHands);
        
        if(posWinners[0] && posWinners[1])
            return 0;   //tied
        else if(posWinners[0])
            return 1;   //P1 won
        else
            return 2;   //P2 won
    }
    
}
