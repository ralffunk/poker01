package concepts;

import java.util.ArrayList;
import java.util.List;

/**
 * An object of type Hand represents a hand of cards.
 * The cards belong to the class Card. A hand is empty when it is created,
 * and a number of cards can be added to it.
 * @author Ralf
 */
public class Hand
{
    /**
     * The cards in the hand.
     */
    private List<Card> hand;
    
    /**
     * This variable holds the limit of cards a hand object can hold.
     * By default it has no limit (limit = -1). It can be set when 
     * constructing the object.
     */
    private final int cardLimit;
    
    /**
     * Create a hand that is initially empty.
     * @param limit is the integer that sets the limit of how many
     * cards this hand can hold (-1 for no limit).
     */
    public Hand(int limit)
    {
        this.hand = new ArrayList<>();
        if(limit < 0)
            this.cardLimit = -1;     //no limit
        else
            this.cardLimit = limit;
    }
    
    /**
     * Returns the card limit that this hand can hold. -1 indicates no limit.
     * @return card limit of this hand
     */
    public int getCardLimit()
    {
        return this.cardLimit;
    }
    
    /**
     * Remove all cards from the hand, leaving it empty.
     */
    public void clear()
    {
        this.hand.clear();
    }
    
    /**
     * Add a card to the hand. It is added at the end of the current hand.
     * @param c the non-null card to be added.
     * @throws NullPointerException if the parameter c is null.
     * @throws IndexOutOfBoundsException if the hand is already full.
     */
    public void addCard(Card c)
    {
        this.addCard(c, this.hand.size());
    }
    
    /**
     * Add a card to the hand.
     * @param c the non-null card to be added.
     * @param pos where to be added
     * @throws NullPointerException if the parameter c is null.
     * @throws IndexOutOfBoundsException if the hand is already full.
     */
    public void addCard(Card c, int pos)
    {
        if(c == null)
        {
            throw new NullPointerException("Can't add a null card to a hand.");
        }
        else if(this.getCardLimit() >= 0)
        {
            if(this.getCardCount() == this.getCardLimit())
            {
                throw new IndexOutOfBoundsException(
                        "This hand is already full to its limit:"
                                + this.getCardLimit());
            }
        }
        this.hand.add(pos, c);
    }
    
    /**
     * Check if the card is in the hand.
     * @param h
     */
    public void addCards(Hand h)
    {
        for(Card c: h.hand)
        {
            this.addCard(c);
        }
    }
    
    /**
     * Returns the number of cards in the hand.
     * @return 
     */
    public int getCardCount()
    {
        return this.hand.size();
    }
    
    /**
     * Check if the card is in the hand.
     * @param c the card
     * @return 
     */
    public boolean containsCard(Card c)
    {
        for(Card card: this.hand)
        {
            if(card.getRank() == c.getRank() && card.getSuit() == c.getSuit())
                return true;
        }
        return false;
    }
    
    /**
     * Remove a card from the hand, if present.
     * @param c the card to be removed.  If c is null or if the card is not in 
     * the hand, then nothing is done.
     */
    public void removeCard(Card c)
    {
        this.hand.remove(c);
    }
    
    /**
     * Remove the card in a specified position from the hand.
     * @param position the position of the card that is to be removed, where
     * positions are starting from zero.
     * @throws IllegalArgumentException if the position does not exist in
     * the hand, that is if the position is less than 0 or greater than
     * or equal to the number of cards in the hand.
     */
    public void removeCard(int position)
    {
        if (position < 0 || position >= this.hand.size())
        {
            throw new IllegalArgumentException("Pos does not exist in hand: "
                    + position);
        }
        this.hand.remove(position);
    }
    
    /**
     * Gets the card in a specified position in the hand.  (Note that this card
     * is not removed from the hand!)
     * @param position the position of the card that is to be returned,
     * starting at 0.
     * @throws IllegalArgumentException if position does not exist in the hand
     * @return
     */
    public Card getCard(int position)
    {
        if (position < 0 || position >= this.hand.size())
        {
            throw new IllegalArgumentException("Position \"" + position
                    + "\" does not exist in hand of \"" + this.hand.size()
                    + "\" cards.");
        }
        return this.hand.get(position);
    }
    
    /**
     * Gets all cards of the hand.
     * @return
     */
    public List<Card> getCards()
    {
        return this.hand;
    }
    
    /**
     * Sorts the cards in the hand so that cards of the same suit are
     * grouped together, and within a suit the cards are sorted by rank.
     * Note that aces are considered to have the highest value, 14.
     */
    public void sortBySuit()
    {
        List<Card> newHand = new ArrayList<>();
        while (this.hand.size() > 0)
        {
            int pos = 0;  // Position of minimal card.
            Card c = this.hand.get(0);  // Minimal card.
            for (int i = 1; i < this.hand.size(); i++)
            {
                Card c1 = this.hand.get(i);
                if (c1.getSuit() < c.getSuit() ||
                        (c1.getSuit() == c.getSuit()
                        && c1.getRank() < c.getRank()))
                {
                    pos = i;
                    c = c1;
                }
            }
            this.hand.remove(pos);
            newHand.add(c);
        }
        this.hand = newHand;
    }
    
    /**
     * Sorts the cards in the hand so that cards of the same value are
     * grouped together.  Cards with the same value are sorted by suit.
     * Note that aces are considered to have the highest value, 14.
     */
    public void sortByValue()
    {
        List<Card> newHand = new ArrayList<>();
        while (this.hand.size() > 0)
        {
            int pos = 0;  // Position of minimal card.
            Card c = this.hand.get(0);  // Minimal card.
            for (int i = 1; i < this.hand.size(); i++)
            {
                Card c1 = this.hand.get(i);
                if (c1.getRank() < c.getRank() || 
                        (c1.getRank() == c.getRank()
                        && c1.getSuit() < c.getSuit()))
                {
                    pos = i;
                    c = c1;
                }
            }
            this.hand.remove(pos);
            newHand.add(c);
        }
        this.hand = newHand;
    }
    
    /**
     * Returns a string that contains all the card.toString() of all cards
     * in the hand. Each card has a new line character at the end.
     * @return string with the textual representation of all cards of the hand
     */
    @Override
    public String toString()
    {
        String s = "{";
        for(int i=0; i < this.getCardCount(); i++)
        {
            s += this.getCard(i).toString() + ", ";
        }
        if(s.length() > 1)
            s = s.substring(0, s.length()-2) + "}";
        
        return s;
    }
    
}
