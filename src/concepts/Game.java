package concepts;

import gui.PokerGUI;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import utils.Logger;

/**
 * A game.
 * @author Ralf
 */
public class Game
{
    /**
     * Class constants.
     */
    public static final int TOTAL_MONEY = 80000,
            SMALL_BLIND = 100,
            BIG_BLIND = SMALL_BLIND * 2,
            SMALL_BET = BIG_BLIND,
            BIG_BET = SMALL_BET * 2,
            MAX_QTY_RAISES = 5;
    
    /**
     * Attributes.
     */
    private final Logger logger;
    private final List<Player> players;
    private final Player player1,
            player2;
    private long startTime, endTime;
    private int roundCounter;
    private final boolean withGUI;
    private final List<JLabel> cardLabels;
    private final JLabel labelRound,
            labelPot,
            labelAmountToCall;
    private final Map<String, Double> preFlopOdds;
    
    /**
     * Constructor.
     * @param p1
     * @param p2
     */
    public Game(Player p1, Player p2)
    {
        this(new Logger(""), new LinkedList<>(Arrays.asList(p1, p2)),
                null, null, null, null);
    }
    
    /**
     * Constructor.
     * @param logger
     * @param p1
     * @param p2
     */
    public Game(Logger logger, Player p1, Player p2)
    {
        this(logger, new LinkedList<>(Arrays.asList(p1, p2)),
                null, null, null, null);
    }
    
    /**
     * Constructor.
     * @param logger
     * @param players
     */
    public Game(Logger logger, List<Player> players)
    {
        this(logger, players, null, null, null, null);
    }
    
    /**
     * Constructor.
     * @param logger
     * @param players
     * @param labelRound
     * @param cardLabels
     * @param labelPot
     * @param labelAmountToCall 
     */
    public Game(Logger logger, List<Player> players, JLabel labelRound,
            List<JLabel> cardLabels, JLabel labelPot, JLabel labelAmountToCall)
    {
        if(players.size() != 2)
        {
            throw new RuntimeException("this game only works for 2 players");
        }
        
        this.logger = logger;
        this.players = players;
        //assign both players to extra variables for easier statistics printing
        this.player1 = this.players.get(0);
        this.player2 = this.players.get(1);
        this.roundCounter = 0;
        
        this.labelRound = labelRound;
        this.cardLabels = cardLabels;
        this.labelPot = labelPot;
        this.labelAmountToCall = labelAmountToCall;
        this.preFlopOdds = Game.loadOddsMap();
        
        this.withGUI = (this.cardLabels != null);
        
        this.gui_initialize();
    }
    
    /**
     * Play the game.
     * @param roundLimit
     * @return 
     */
    public String[] play(int roundLimit)
    {
        this.logger.setGame(1);
        this.startTime = System.currentTimeMillis();
        
        boolean gameIsOver = false;
        int playersStillPlaying;
        
        //initialize players for game
        for(Player p: this.players)
        {
            p.setForGameStart(Game.TOTAL_MONEY / this.players.size());
        }
        
        while( !gameIsOver )
        {
            this.rotateDealer();
            
            Round r = new Round(this.logger, this.players, this.preFlopOdds,
                    this.getNextRoundNumber(), this.labelRound, this.cardLabels,
                    this.labelPot, this.labelAmountToCall);
            r.play();
            
            this.logStatus();   //write to log
            
            //count players still playing
            playersStillPlaying = 0;
            for(Player p: this.players)
            {
                if( !p.hasLostGame() )
                    playersStillPlaying += 1;
            }
            if(playersStillPlaying < 2 ||
                    (roundLimit > 0 && this.roundCounter >= roundLimit))
            {
                gameIsOver = true;
            }
            
            //the User can decide if he wants to continue to next round
            //this is also to show result from the last round in GUI
            if( !gameIsOver )
            {
                for(Player p: this.players)
                {
                    p.gui_showOdds(true);
                }
                for(Player p: this.players)
                {
                    if( !p.goForNextRound() )
                        gameIsOver = true;
                }
            }
        }
        
        //update the players labels at end of game
        for(Player p: this.players)
        {
            p.updateGUI();
        }
        
        this.endTime = System.currentTimeMillis();
        this.logger.unsetGame(this.getGameDuration());
        this.logger.close();
        
        return this.getEndOfGameInfo();
    }
    
    /**
     * Shift the dealer position to the next player.
     * The dealer is the last person in players list.
     */
    private void rotateDealer()
    {
        Collections.rotate(this.players, 1);
    }
    
    /**
     * Returns the game duration as string.
     */
    private String getGameDuration()
    {
        return String.format("%,d ms", this.endTime - this.startTime);
    }
    
    /**
     * Returns the next round number.
     */
    private int getNextRoundNumber()
    {
        this.roundCounter++;
        return this.roundCounter;
    }
    
    /**
     * Write to log.
     */
    private void logStatus()
    {
        //ONLY for 2 players
        this.logger.print(String.format("%-13s (%s = %5d) (%s = %5d)",
                "players:", this.player1, this.player1.getMoney(), this.player2,
                this.player2.getMoney()));
    }
    
    /**
     * Returns some information at the end of the game.
     */
    private String[] getEndOfGameInfo()
    {
        //ONLY for 2 players
        if(this.player1.getMoney() < this.player2.getMoney())
        {
            this.player1.increaseGamesLost();
            this.player2.increaseGamesWon();
        }
        else if(this.player1.getMoney() > this.player2.getMoney())
        {
            this.player1.increaseGamesWon();
            this.player2.increaseGamesLost();
        }
        else
        {
            this.player1.increaseGamesWon();
            this.player2.increaseGamesWon();
        }
        
        return new String[]{
            String.format("%,d rounds in %s", this.roundCounter, this.getGameDuration()),
            this.player1.getGameStatistics(),
            this.player2.getGameStatistics()};
    }
    
    /**
     * Updates GUI.
     */
    private void gui_initialize()
    {
        if(this.withGUI)
        {
            //initialize players for game, so that money labels show the money
            for(Player p: this.players)
            {
                p.setForGameStart(Game.TOTAL_MONEY / this.players.size());
            }
            
            ImageIcon img = new javax.swing.ImageIcon( getClass()
                    .getResource("/resources/" + PokerGUI.BACK_OF_CARDS_IMAGE_FILE));
            for(JLabel l: this.cardLabels)
            {
                l.setIcon(img);
            }
            
            this.labelRound.setText("Round 0");
            this.labelPot.setText("0 (0.0 %)");
            this.labelAmountToCall.setText("0 (0.0 %)");
        }
    }
    
    /**
     * Load the map with pre-flop odds.
     * @return 
     */
    public static Map<String, Double> loadOddsMap()
    {
        Map<String, Double> map = new LinkedHashMap<>();
        
        String fileName = "/src/resources/preflop_prob.txt";
        String filePath = new File("").getAbsolutePath();
        try
        {
            try(BufferedReader br = new BufferedReader(new FileReader(filePath + fileName)))
            {
                String delims = "[ %\t]+";
                for(String s = br.readLine(); s != null; s = br.readLine())
                {
                    String[] tokens = s.split(delims);
                    map.put(tokens[0], Double.parseDouble(tokens[1]));
                }
            }
        }
        catch(IOException | NumberFormatException ex)
        {
            
        }
        
        return map;
    }
}
